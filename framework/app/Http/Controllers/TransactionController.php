<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Datastatik;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class TransactionController extends Controller
{

	public $datastatic;
    public $client;

    public $end_week;
    public $start_week;
    public $last_day;

	 public function __construct()
    {
        $this->datastatic = new Datastatik();
        $this->client = new Client(['base_uri' => $this->datastatic->pointing()]);
    }

    public function paymentMethod($id, Request $request){
    	 $promises = [
            'paymentsummary' => $this->client->getAsync('api/customer/view/order/payment/summary?cpyid='.$id, [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___')
                    // 'chat-session' => "Bearer ".$request->session()->get('chat-session')
                ],
                // 'form_params' => [
                //     'cpyid' => $cpid
                //  ]

            ])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'paymentsummary' => json_decode($results['paymentsummary']['value']->getBody())
        );

         $data = array(
            'paymentsummary' => $hasilapi['paymentsummary']
        );
        //  dd($data);
    	return view('payment.payment_method')->with($data);
    }

    public function paymentMethodProses(Request $request){
        $promises = [
            'paymentproses' => $this->client->postAsync('api/customer/add/order/payment/method', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___')
                    // 'chat-session' => "Bearer ".$request->session()->get('chat-session')
                ],

                'form_params' => [
                    'cpyid' => $request->cpid,
                    'method' => $request->input('tab2'),
                 ]

            ])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'paymentproses' => json_decode($results['paymentproses']['value']->getBody())
        );

         $data = array(
            'paymentproses' => $hasilapi['paymentproses']
        );
        //   dd($data);
        return redirect('booking-complite/'.$request->cpid);
    }
}

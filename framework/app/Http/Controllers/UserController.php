<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Datastatik;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class UserController extends Controller
{

    public function __construct()
    {
        $this->datastatic = new Datastatik();
        $this->client = new Client(['base_uri' => $this->datastatic->pointing()]);
    }

    public function index(Request $request){

        $promises = [
            'userdata' => $this->client->getAsync('api/customer/view/data', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ]
            ]),
            'branddata' => $this->client->getAsync('api/customer/view/brand/all', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ]
            ])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'userdata' => json_decode($results['userdata']['value']->getBody()),
            'branddata' => json_decode($results['branddata']['value']->getBody()),
        );

        $data = array(
            'userdata' => $hasilapi['userdata'],
            'branddata' => $hasilapi['branddata']
        );

        //dd($hasilapi['userdata']);

        if ($hasilapi['userdata']->status == 1) {
            return view('user.user_profile')->with($data);
        } else {
            return redirect('/')->with('error_code', 5);
        }
    }
    
    // public function userEditPhotoProses(Request $request){
    //     $promises = [
    //         'usereditphoto' => $this->client->postAsync('api/customer/edit/data/pic/photo', [
    //             'headers' => [
    //                 'Authorization' => "Bearer ".$request->session()->get('my_token___'),
    //             ],
                
    //             'form_data' => [
    //                 'pic_photo' => $request->input('getUserImg')
    //             ]

                
    //         ])
    //     ];

    //     $results = Promise\unwrap($promises);

    //     $results = Promise\settle($promises)->wait();

    //     $hasilapi = array(
    //         'usereditphoto' => json_decode($results['usereditphoto']['value']->getBody()),
    //     );

    //     //dd($hasilapi['userdata']);
    //     return redirect();
    // }
}

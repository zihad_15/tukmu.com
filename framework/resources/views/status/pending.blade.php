@inject('pointing', 'App\Helpers\Datastatik')
@extends('layouts.master')
@section('st_css')

	@include('booking.blocks.header')

	<style type="text/css">
		.p-r-li {
			padding-left: 63px;
			padding-right: 63px;
			background-color: #bababa;
			color: white;
		}

		.jumbotron {
			padding: 0;
		}

		.f-p-s {
			font-size: 10px;
		}

		.f-s-13 {
			font-size: 13px;
		}

		.nav-pills .nav-link.active,
		.nav-pills .show > .nav-link {
		  color: #fff;
		  background-color: orange;
		}
	</style>
@stop
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8" style="position: relative; margin: auto; display: block;">
				<ul class="nav nav-pills mb-3" style="margin-top: 80px;" id="pills-tab" role="tablist">
				  <li class="nav-item">
				    <a class="nav-link p-r-li active" style="margin-right: 3px; color: white;" href="#">NEGO</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link p-r-li" style="margin-right: 3px; color: white;" href="{{ url('cicilan') }}">CICILAN</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link p-r-li" style="color: white;" href="{{ url('tagihan') }}">TAGIHAN</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link p-r-li" style="margin-left: 3px; color: white;" href="{{ url('lunas') }}">LUNAS</a>
				  </li>
				</ul>
				<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
				  	<div class="jumbotron-fluid" style="background-color: darkblue; color: white;">
				  		<div class="container" style="text-align: center;">
					  		<h5>
					  			<img src="assets/master/img/tandaseru2.png" width="20" height="20">&nbsp;&nbsp; PESANAN ANDA TELAH KAMI TERIMA &nbsp;&nbsp;<img src="assets/master/img/tandaseru2.png" width="20" height="20">
					  		</h5>
					  		<p style="font-size: 13px;"><b>Kami akan memberikan notifikasi jika pihak penyelenggara sudah memberikan jawaban.</b></p>
				  		</div>
					</div>
						<div id="pending"></div>
						{{-- @foreach($pending->pending_booking_list as $key => $value)
						@if(isset($value->customer_brand->custbrandid))
				  	<div class="jumbotron-fluid">
							<div class="container">
								<div class="row">
									<div class="col-md-4">
										<img src="http://tukbase.bukapameran.com/picGet/{{ $value->space_detail[0]->first_media->spacemediapath }}" width="230px" height="230px" style="margin-top: 15px;">
									</div>
									<div class="col-md-6">
										<br>
										<h5>{{ $value->space_detail[0]->eventname }}</h5>
										<div class="row">
											<p class="f-p-s" style="margin-left: 15px;"><img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; {{ $value->space_detail[0]->startdate }} - {{ $value->space_detail[0]->finishdate }}</p>
											<p class="f-p-s" style="margin-left: 20px;"><img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; {{ $value->space_detail[0]->spacename }}</p>
										</div>
										<span class="f-s-13">STAN</span>
										<p>{{ $value->payment_grid_with_code[0]->code }}</p>
										<hr style="margin-top: -1em;">

										@If(isset($value->customer_brand->brandname))
										<span class="f-s-13">BRAND</span>
										<p>{{ $value->customer_brand->brandname }}</p>
										<hr style="margin-top: -1em;">
										@endif

										<span class="f-s-13">PENGELOLA / EVENT ORGANIZER</span>
										<p>{{ $value->space_detail[0]->space_provider->spaceprovname }}</p>
										<hr style="margin-top: -1em;">
									</div>
									<div class="col-md-2">
										<div id="notif_chat"
										<a href="{{ url('booking/'.$value->custpaymentid.'/'.$value->customer_brand->custbrandid) }}" class="btn btn-defaut" style="background-color: orange; color: white; margin-top: 200px;">NEGO</a>
									</div>
								</div>
								<hr style="background-color: orange;">
							</div>
						</div>
						@endif
				  	@endforeach --}}
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
@stop


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>

{{-- 
<script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script> --}}

<script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>

<script id="pending_room" type="text/babel" src="{{url('/')}}/assets/master/jsx/pending.jsx?endpoint={{ $pointing->pointing() }}&acces_token={{session('my_token___')}}&url={{url('/')}}&session_chat={{session('chat_session__')}}"></script>

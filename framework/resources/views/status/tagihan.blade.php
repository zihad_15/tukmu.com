@extends('layouts.master')
@section('st_css')

	@include('booking.blocks.header')

	<style type="text/css">
		.p-r-li {
			padding-left: 63px;
			padding-right: 63px;
			background-color: #bababa;
			color: white;
		}

		.jumbotron {
			padding: 0;
		}

		.f-p-s {
			font-size: 10px;
		}

		.f-s-13 {
			font-size: 13px;
		}

		.statushead {
			background-color: #bababa;
			padding: 8px 80px 8px 50px;
			border-radius: 3px;
		}
		
        .lokasi-list {
            display: flex;
            padding: 0px;
            margin-top: -4%;
        }
        .lokasi-list li {
            list-style: none;
            text-decoration: none;
            margin-right: 5px;
        }
	</style>
@stop
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8" style="position: relative; margin: auto; display: block;">
				<ul class="nav nav-pills mb-3" style="margin-top: 80px;" id="pills-tab" role="tablist">
				  	<div class="row">
					  	<div class="col-md-3">
					  		<a href="{{ url('pending-order') }}" style="text-decoration: none;">
						  		<div class="statushead">
						  			<p style="color: white;text-align: center;display: table;margin: auto;">NEGO</p>
						  		</div>
					  		</a>
					  	</div>
					  	<div class="col-md-3">
					  		<a href="{{ url('cicilan') }}" style="text-decoration: none;">
						  		<div class="statushead">
						  			<p style="color: white;text-align: center;display: table;margin: auto;">CICILAN</p>
						  		</div>
						  	</a>
					  	</div>
					  	<div class="col-md-3">
					  		<a href="#" style="text-decoration: none;">
						  		<div style="background-color: orange; padding: 8px 80px 8px 50px; border-radius: 3px">
						  			<p style="color: white;text-align: center;display: table;margin: auto;">TAGIHAN</p>
						  		</div>
						  	</a>
					  	</div>
					  	<div class="col-md-3">
					  		<a href="{{ url('lunas') }}" style="text-decoration: none;">
						  		<div class="statushead">
						  			<p style="color: white;text-align: center;display: table;margin: auto;">LUNAS</p>
						  		</div>
						  	</a>
					  	</div>
					</div>
				</ul>
				<div class="tab-content" id="pills-tabContent">
				  <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
				  	<dir class="jumbotron jumbotron-fluid" style="
				  	background-color: red; color: white;">
				  		<div class="container" style="text-align: center;">
					  			<h5>
					  				<img src="assets/master/img/tandaseru2.png" width="20" height="20">&nbsp;&nbsp;
					  				LAKUKAN PEMBAYARAN 
					  				&nbsp;&nbsp;<img src="assets/master/img/tandaseru2.png" width="20" height="20">
					  			</h5>
					  			<p style="font-size: 13px;"><b>Segera lakukan pembayaran untuk melanjutkan ke tahap selanjutnya.</b></p>
				  		</div>
				  	</dir>
				  	@foreach($tagihan->pending_booking_list as $key => $value)
				  	<div class="jumbotron jumbotron-fluid">
					  <div class="container">
					    <div class="row">
					    	<div class="col-md-4">
					    		<img src="http://tukbase.bukapameran.com/picGet/{{ $value->space_detail[0]->first_media->spacemediapath }}" width="230px" height="230px" style="margin-top: 15px;">
					    	</div>
						    <div class="col-md-6">
						    	<h5>{{ $value->space_detail[0]->eventname }}</h5>
						    	<div class="row">
						    		<p class="f-p-s" style="margin-left: 15px;"><img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; {{ $value->space_detail[0]->startdate }} - {{ $value->space_detail[0]->finishdate }}</p>
						    		<p class="f-p-s" style="margin-left: 20px;"><img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; {{ $value->space_detail[0]->spacename }}</p>
						    	</div>
						    	<p class="f-s-13">STAN</p>
        			    		    <ul class="lokasi-list">
        			    			<?php
        				    			$length = count($value->payment_grid_with_code);
        								for ($i = 0; $i < $length; $i++) {
        									echo "<li>";
        									if($length == $i){
        								  	    echo $value->payment_grid_with_code[$i]->code;
        									} else {
        								  	    echo $value->payment_grid_with_code[$i]->code.",";
        									}
        								  	echo "</li>";
        								} 
        							?>
        			    		</ul>
						    	<hr style="margin-top: -1em;">

						    	<span class="f-s-13">BRAND</span>
						    	<p>{{ $value->customer_brand->brandname }}</p>
						    	<hr style="margin-top: -1em;">

						    	<span class="f-s-13">PENGELOLA / EVENT ORGANIZER</span>
						    	<p>{{ $value->space_detail[0]->space_provider->spaceprovname }}</p>
						    	<hr style="margin-top: -1em;">
						    </div>
						    <div class="col-md-2">
						    	<button style="margin-top: 35px;" class="btn btn-defaut" onclick="document.getElementById('getUserImg').click()"><span style="font-size: 13px;">Unggah Foto</span></button>
                				<input type='file' id="getUserImg" style="display:none;" required="required">
                				<br>
                				<br>
                				<br>
                				<br>
                				<br>
                				<a href="{{ url('booking/'.$value->custpaymentid.'/'.$value->custbrandid) }}" style="font-size: 12px; color: black; margin-left: 15px;"><img src="{!! asset('assets/master/img/chat.png') !!}" width="18px" height="18px">&nbsp; CHAT</a>
						    	<a href="{{ url('metode-pembayaran/'.$value->custpaymentid) }}" class="btn btn-defaut" style="background-color: red; color: white; margin-top: 10px; margin-left: 10px;">BAYAR</a>
						    </div>
					    </div>
					  </div>
					</div>
					@endforeach
				  </div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>

	<script type="text/javascript">
		$('#dropdownMenu a').on('click', function(){
	    $('#pills-tab li a.active').removeClass('active');
	    $(this).addClass('active');
});
	</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@stop
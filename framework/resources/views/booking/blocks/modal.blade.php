
    <!-- MODAL SECTION -->

    <!-- MODAL REGISTER -->
    <div class="modal fade" id="modalregister" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: orange;">
                    <ul class="nav nav-pills2" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#userregister" role="tab" aria-controls="home" aria-selected="true" style="color: black;">DAFTAR PENGGUNA</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#adminregister" role="tab" aria-controls="profile" aria-selected="false" style="color: black;">DAFTAR PENYELENGGARA</a>
                      </li>
                    </ul>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="userregister" role="tabpanel" aria-labelledby="home-tab">
                        <center>
                            <h4>DAFTAR <strong>PENGGUNA</strong></h4>
                        </center>
                        <br>
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Nama Lengkap">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="repassword" placeholder="Ulangi Kata Sandi">
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-primary">DAFTAR</button>
                                </div>
                            </div>
                        </form>
                      </div>
                      <div class="tab-pane fade" id="adminregister" role="tabpanel" aria-labelledby="profile-tab">
                            <h4>APAKAH ANDA <strong>PENYELENGGARA ACARA?</strong></h4>
                            <br>
                            <p>Pastikan Anda terhubung dengan para tenan dari pasar lokan dan internasional secara cepat!</p>
                            <br>
                            <a href="#" class="btn btn-primary">DAFTAR</a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL LOGIN -->
    <div class="modal fade" id="modallogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: orange;">
                    <ul class="nav nav-pills2" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#userlogin" role="tab" aria-controls="home" aria-selected="true" style="color: black;">LOGIN PENGGUNA</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#adminlogin" role="tab" aria-controls="profile" aria-selected="false" style="color: black;">LOGIN PENYELENGGARA</a>
                      </li>
                    </ul>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="userlogin" role="tabpanel" aria-labelledby="home-tab">
                        <center>
                            <h4>LOGIN <strong>PENGGUNA</strong></h4>
                        </center>
                        <br>
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" placeholder="Nama Pengguna">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-success">LOGIN</button>
                                </div>
                                <div class="col">
                                    <p>Lupa Kata Sandi? <a href="#modalpr" data-dismiss="modal" data-toggle="modal">Klik disini!</a></p>
                                </div>
                            </div>
                        </form>
                      </div>
                      <div class="tab-pane fade" id="adminlogin" role="tabpanel" aria-labelledby="profile-tab">
                        <center>
                            <h4>LOGIN <strong>PENYELENGGARA</strong></h4>
                        </center>
                        <br>
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" placeholder="Nama Penyelenggara">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-success">LOGIN</button>
                                </div>
                                <div class="col">
                                    <p>Lupa Kata Sandi? <a href="">Klik disini!</a></p>
                                </div>
                            </div>
                        </form>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL PASSWORD RECOVERY -->
    <div class="modal fade" id="modalpr" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: orange;">
                    <ul class="nav nav-pills2" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#userlogin" role="tab" aria-controls="home" aria-selected="true" style="color: black;">PEMULIHAN KATA SANDI PENGGUNA</a>
                      </li>
                    </ul>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="pr" role="tabpanel" aria-labelledby="home-tab">
                        <h4>APAKAH ANDA <strong>LUPA KATA SANDI?</strong></h4>
                        <p>Silahkan masukkan email yang sudah terdaftar untuk akun anda.</p>
                        <br>
                        <br>
                        <form>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Contoh : pengguna@gmail.com">
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-warning">Kirim Link Recovery</button>
                                </div>
                                <div class="col">
                                    <p>Mengingat Kata Sandi Anda? <a href="#modallogin" data-dismiss="modal" data-toggle="modal">Klik disini!</a></p>
                                </div>
                            </div>
                        </form>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
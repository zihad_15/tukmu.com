
<footer class="footer bg-primary">
    <div class="container bottom_border">
        <div class="row">
            <div class=" col-sm-3 col-md col-sm-4  col-12 col">
                <h3 class="headin5_amrc col_white_amrc pt2">About Us</h3>
<!--headin5_amrc-->
                <p class="mb10" style="color: black;">With more than 15 years of experience we can proudly say that we are one of the best in business, a trusted supplier for more than 1000 companies.</p>


            </div>


            <div class=" col-sm-3 col-md  col-6 col">
                <h3 class="headin5_amrc col_white_amrc pt2">Site Map</h3>
                <!--headin5_amrc-->
                <ul class="footer_ul_amrc">
                <li><a href="#" style="color: black;">About</a></li>
                <li><a href="#" style="color: black;">Bantuan</a></li>
                <li><a href="#" style="color: black;">Daftar</a></li>
                <li><a href="#" style="color: black;">Masuk</a></li>
                </ul>
            <!--footer_ul_amrc ends here-->
            </div>

            <div class=" col-sm-3 col-md  col-6 col">
                <h3 class="headin5_amrc col_white_amrc pt2">Get In Touch</h3>

                    <a href="#"><i class="fab fa-facebook-f" style="margin-right: 15px;"></i></a>
                    <a href="#"><i class="fab fa-twitter" style="margin-right: 15px;"></i></a>
                    <a href="#"><i class="fab fa-linkedin" style="margin-right: 15px;"></i></a>
                    <a href="#"><i class="fab fa-instagram" style="margin-right: 15px;"></i></a>
            </div>

            <div class=" col-sm-3 col-md  col-12 col">
                <h3 class="headin5_amrc col_white_amrc pt2">Newsletter</h3>
                <p style="color: black;">Subscribe us for new update information of our event</p>
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subscribe" placeholder="Enter your email here">
                    </div>
                    <button type="submit" class="btn btn-defaullt btn-sm" style="background-color: #f47b50; color: white;">SUBSCRIBE</button>
                </form>
            </div>
        </div>
    </div>


    <div class="container">
        <!--foote_bottom_ul_amrc ends here-->
        <p class="text-center" style="color: black;">Copyright @2017 | Designed With by <a href="#">tukmu.com</a></p>

        
        <!--social_footer_ul ends here-->
    </div>
</footer>

    <!-- MODAL SECTION -->

    <!-- MODAL SETUJU -->
    <div class="modal fade" id="modalconfirmsetuju" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header p-r bg-orange">
                        <div class="c-white p-10 div-c">
                            <p class="t-a-c m-r">Confirm deal harga</p>
                        </div>
                    </div>
                    <div class="modal-body bg-second c-second">
                        <p class="t-a-c">
                            Anda memilih untuk menyetujui deal harga dengan penyelenggara ini
                        </p>
                        <p class="t-a-c">
                            Apakah anda yakin dengan pilihan anda?
                        </p>
                        <div class="d-flex justify-content-center">
                            <div class="p-2">
                                <button class="btn btn-pesan b-radius-15 b-r bg-green f-s-12 c-white" data-dismiss="modal" data-toggle="modal" data-target="#modalconfirm">
                                    IYA
                                </button>
                            </div>
                            <div class="p-2">
                                <button class="btn btn-pesan b-radius-15 b-r bg-red f-s-12 c-white" data-dismiss="modal">
                                    TIDAK
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    
    <!-- MODAL TIDAK SETUJU -->
    <div class="modal fade" id="modalconfirmtidaksetuju" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header p-r bg-orange">
                        <div class="c-white p-10 div-c">
                            <p class="t-a-c m-r">Confirm tidak deal harga</p>
                        </div>
                    </div>
                    <div class="modal-body bg-second c-second">
                        <p class="t-a-c">
                            Anda memilih untuk tidak menyetujui deal harga dengan penyelenggara ini
                        </p>
                        <p class="t-a-c">
                            Apakah anda yakin dengan pilihan anda?
                        </p>
                        <div class="d-flex justify-content-center">
                            <div class="p-2">
                                <button class="btn btn-pesan b-radius-15 b-r bg-green f-s-12 c-white">
                                    IYA
                                </button>
                            </div>
                            <div class="p-2">
                                <button class="btn btn-pesan b-radius-15 b-r bg-red f-s-12 c-white" data-dismiss="modal">
                                    TIDAK
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    
    <!-- MODAL CONFIRM -->
    <div class="modal fade" id="modalconfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header p-r bg-orange">
                        <div class="c-white p-10 div-c">
                            <p class="t-a-c m-r">A10</p>
                        </div>
                    </div>
                    <div class="modal-body bg-second c-second">
                        <div class="d-flex justify-content-start">
                            <div class="p-2 p-r-l w-150">
                                Harga Total (Rp)
                            </div>
                            <div class="p-2">
                                100.000.000
                            </div>
                        </div>
                        <div class="d-flex justify-content-start">
                            <div class="p-2 p-r-l w-150">
                                Down Payment
                            </div>
                            <div class="p-2">
                                30.000.000
                            </div>
                        </div>
                        <div class="d-flex justify-content-start">
                            <div class="p-2 p-r-l w-150">
                                Batas akhir tenor
                            </div>
                            <div class="p-2">
                                10 November 2018
                            </div>
                        </div>
                        <div class="d-flex justify-content-start">
                            <div class="p-2 p-r-l w-150">
                                Tenor
                            </div>
                            <div class="p-2">
                                10 Kali
                            </div>
                        </div>
                        <div class="d-flex justify-content-start">
                            <div class="p-2 p-r-l w-150">
                                Tanggal Ditagih
                            </div>
                            <div class="p-2">
                                <div class="table-responsive table-date-pesan">
                                    <table class="table table-striped c-white">
                                        <tr class="bg-orange t-a-c">
                                            <td>Tanggal</td>
                                            <td>Bulan</td>
                                            <td>Tahun</td>
                                        </tr>
                                        <tr class="t-a-c">
                                            <td>10</td>
                                            <td>11</td>
                                            <td>2018</td>
                                        </tr>
                                        <tr class="t-a-c">
                                            <td>12</td>
                                            <td>11</td>
                                            <td>2018</td>
                                        </tr>
                                        <tr class="t-a-c">
                                            <td>13</td>
                                            <td>11</td>
                                            <td>2018</td>
                                        </tr>
                                        <tr class="t-a-c">
                                            <td>14</td>
                                            <td>11</td>
                                            <td>2018</td>
                                        </tr>
                                        <tr class="t-a-c">
                                            <td>15</td>
                                            <td>11</td>
                                            <td>2018</td>
                                        </tr>
                                        <tr class="t-a-c">
                                            <td>20</td>
                                            <td>11</td>
                                            <td>2018</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr class="b-orange m-r">
                        <div class="d-flex justify-content-start">
                            <div class="p-2 p-r-l w-200">
                                TAGIHAN PER TENOR
                            </div>
                            <div class="p-2">
                                Rp. 7.000.000
                            </div>
                        </div>
                        <hr class="b-orange m-r">
                    </div>
                </div>
            </div>
        </div>
    
    <!-- MODAL BATAL PESANAN -->
    <div class="modal fade" id="modalconfirmbatalpesanan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header p-r bg-orange">
                        <div class="c-white p-10 div-c">
                            <p class="t-a-c m-r">Confirm batal pesanan</p>
                        </div>
                    </div>
                    <div class="modal-body bg-second c-second">
                        <p class="t-a-c">
                            Anda memilih untuk batal memesan<br/> dengan penyelenggara ini
                        </p>
                        <p class="t-a-c">
                            Apakah anda yakin dengan pilihan anda?
                        </p>
                        <div class="d-flex justify-content-center">
                            <div class="p-2">
                                <button class="btn btn-pesan b-radius-15 b-r bg-green f-s-12 c-white" data-dismiss="modal" data-toggle="modal" data-target="#modalyesbatal">
                                    IYA
                                </button>
                            </div>
                            <div class="p-2">
                                <button class="btn btn-pesan b-radius-15 b-r bg-red f-s-12 c-white" data-dismiss="modal">
                                    TIDAK
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <!-- MODAL YES BATAL PESANAN -->
    <div class="modal fade" id="modalyesbatal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header p-r bg-orange">
                        <div class="c-white p-10 div-c">
                            <p class="t-a-c m-r">Confirm batal pesanan</p>
                        </div>
                    </div>
                    <div class="modal-body bg-second c-second">
                        <p class="t-a-c">
                            Penyelenggara memilih untuk<br/> membatalkan pesanan anda karena
                        </p>
                        <p class="t-a-c c-red">
                            Apakah anda yakin dengan pilihan anda?
                        </p>
                        <div class="d-flex justify-content-center">
                            <div class="p-2">
                                <button class="btn btn-pesan b-radius-15 b-r bg-green f-s-12 c-white" data-dismiss="modal">
                                    OK
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
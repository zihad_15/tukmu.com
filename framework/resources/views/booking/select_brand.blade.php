@extends('layouts.master')
@section('content')

@include('booking.blocks.header')
	 
	 <style type="text/css">
	 	.jumbotron {
	 		padding: 20px;
	 	}
	 	
        .lokasi-list {
            display: flex;
            padding: 0px;
            margin-top: -4%;
        }
        .lokasi-list li {
            list-style: none;
            text-decoration: none;
            margin-right: 5px;
            font-weight: bold;
            text-decoration: underline;
            color: darkorange;
        }
	 </style>

	<div class="container" style="margin-top: 100px;">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="jumbotron">
				  <div class="container">
				  	<h2>PILIH BRAND</h2>
				  	<div class="row">
				  		<div class="col-md-6">
				  			<select class="form-control brand_id" required="required">
				  				<option value="">Pilih Brand</option>
				  				@foreach ($displaybrand->brand_list as $key => $value)
				  				<option value="{{ $value->brand_id }}">{{ $value->brand_name }}</option>
				  				@endforeach
				  			</select>
				  		</div>
				  		<div class="col-md-6">
				  			@if(count($displaybrand->brand_list) == 5)
				  			BRAND TELAH MENCAPAI BATAS MAKSIMAL
				  			@else
				  			<a href="{{ url('daftar-brand-pengguna') }}" style="color: black;"><img src="{!! asset('assets/master/img/tambah.png') !!}" width="12" height="12"> BUAT / TAMBAH BRAND BARU</a>
				  			@endif
				  		</div>
				  	</div>
				  	<hr style="background-color: orange;">
				  	<div class="row">
				  		<div class="col-md-6">
				  			<h2>JUDUL</h2>
				  			<div class="d-flex">
		                        <div class="p-2 p-r-l">
		                            <i class="fa fa-calendar-alt c-orange" style="color: orange;"></i>
		                        </div>
		                        <div class="p-2 p-r-l">
								<span><b>{{ date("d-M-Y", strtotime($order_booking->startdate)) }} - {{ date("d-M-Y", strtotime($order_booking->finishdate)) }}</b></span>
		                        </div>
		                    </div>
		                    <div class="d-flex">
		                        <div class="p-2 -r-l">
		                            <i class="fa fa-map-marker-alt c-orange" style="color: orange;"></i>
		                        </div>
		                        <div class="p-2 p-r-l">
		                            <span><b>{{ $order_booking->spacename }}</b></span>
		                        </div>
		                    </div>
		                    <br>
		                    <p><b>STAN</b></p>
		                     <ul class="lokasi-list">
        			    			<?php
        				    			$length = count($order_booking->payment_grid_with_code);
        								for ($i = 0; $i < $length; $i++) {
        									echo "<li>";
        								  	echo $order_booking->payment_grid_with_code[$i]->code.",";echo " ";
        								  	echo "</li>";
        								} 
        							?>
        			    		</ul>

		                    <!--<span><b>BRAND</b></span>-->
		                    <!--<p style="color: orange;"><u><b>NAMA BRAND</b></u></p>-->
				  		</div>
				  		<div class="col-md-6">
				  			<img src="assets/master/img/sample.png" width="260" height="260">
				  		</div>
				  	</div>
				  	<hr style="background-color: orange;">
						<!-- {{-- <center><a href="{{ url('booking') }}/{{$id}}" class="btn btn-primary" style="background-color: #ff9649">LANJUTKAN</a></center> --}} -->
						<center><a href="javascript:;" class="btn btn-primary " id="btn-booking" style="background-color: #ff9649">LANJUTKAN</a></center>
				  </div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

			<script>
				$('document').ready(function(){
					$('#btn-booking').click(function(){
						// console.log($(".brand_id option:selected").val())
						if($(".brand_id option:selected").val() !== ""){
							window.location = "{{ url('booking') }}/{{$id}}"+"/"+$(".brand_id option:selected").val();
						} else {
							alert("Mohon untuk memilih brand anda terlebih dahulu.");
						}
					});
				});
			</script>
		</div>
	</div>
@stop
@inject('pointing', 'App\Helpers\Datastatik')
@extends('booking.layout')

@section('content')

    @include('booking.blocks.header')

    <div id="chat_content"></div>
    {{-- <div class="container content m-t-15 m-b-15">
        <div class="row">
            <div class="col-md-12">
                <h2 class="f-w-300">PEMESANAN</h2>
                <div class="row content-booking">
                    <div class="col-md-6">
                        <div class="bg-white p-15 m-b-15">
                            <div class="row">
                                <div class="col-md-6">
                                    <h2 class="f-w-300">JUDUL</h2>
                                    <br/>
                                    <div class="d-flex">
                                        <div class="p-2 p-r-l">
                                            <i class="fa fa-calendar-alt c-orange"></i>
                                        </div>
                                        <div class="p-2 p-r-l">
                                            <span>{{ $branddetail->startdate }} - {{ $branddetail->finishdate }}</span>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="p-2 p-r-l">
                                            <i class="fa fa-map-marker-alt c-orange"></i>
                                        </div>
                                        <div class="p-2 p-r-l">
                                            <span>{{ $branddetail->spacename }}</span>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="p-2 p-r-l">
                                            <i class="fa fa-dollar-sign c-orange"></i>
                                        </div>
                                        <div class="p-2 p-r-l">
                                            <span>Rp. {{ $branddetail->minprice }} - Rp. {{ $branddetail->maxprice }}</span>
                                        </div>
                                    </div>
                                    <br/>
                                    <h6 class="m-r-b f-s-14">STAN</h6>
                                    <span class="c-orange t-td-u">
                                        {{ $branddetail->payment_grid_with_code[0]->code }}
                                        <div id="stan"></div>
                                    </span>
                                    <hr/>
                                    @if(isset($branddetail->customer_brand->brandname))
                                    <h6 class="m-r-b f-s-14">BRAND</h6>
                                    <span class="c-orange">{{ $branddetail->customer_brand->brandname }}</span>
                                    @endif
                                    <hr/>
                                    <h6 class="m-r-b f-s-14">PENGELOLA</h6>
                                    <span class="c-orange t-td-u">NOTHING</span>
                                    <hr />
                                </div>
                                <div class="col-md-6">
                                    <img class="img-fluid img-thumbnail" src="{{ asset('assets/master/images/beatles.png') }}" />
                                    <br/><br/>
                                    <h6 class="m-r-b">KONDISI & SYARAT</h6>
                                    <a class="c-orange t-td-u" href="#"><span>LIHAT</span></a>
                                </div>
                                <div class="col-md-12">
                                    <h6>TENTANG ACARA</h6>
                                    <p class="f-s-14">{!! $branddetail->desc !!}</p>
                                </div>
                            </div>
                        </div>
                        <div class="bg-white p-15">
                            <h2 class="m-r-b">TAMBAHAN</h2>
                            <span class="c-orange f-s-12"><i>Anda baru bisa memilih tambahan setelah menyelesaikan negosiasi</i></span>

                            <div class="table-responsive-md m-t-15 m-b-15">
                                <table class="table">
                                    <tr class="bg-orange c-white t-a-c">
                                        <td>JENIS</td>
                                        <td>HARGA</td>
                                        <td>SISA</td>
                                        <td>BATAS PESAN</td>
                                    </tr>
                                    <tr class="t-a-c">
                                        <td class="bg-light-orange">Umbul 80 x 190</td>
                                        <td class="bg-light-yellow">Rp. 150.000</td>
                                        <td class="bg-light-orange">5</td>
                                        <td class="bg-light-yellow">01 jan 18</td>
                                    </tr>
                                    <tr class="t-a-c">
                                        <td class="bg-light-orange">Litrik / 2 Amper</td>
                                        <td class="bg-light-yellow">Rp. 150.000</td>
                                        <td class="bg-light-orange">5</td>
                                        <td class="bg-light-yellow">01 jan 18</td>
                                    </tr>
                                </table>
                            </div>

                            <h6 class="m-r-b">Keterangan</h6>
                            <span class="f-s-12">- Harga diatas berlaku dari tanggal 08 Desember - 10 Desember 2017</span>
                        </div>
                    </div>
                    <div class="col-md-6 p-r-l chat-content">
                        <div class="bg-light-black p-15">
                            <div class="b-radius-15 p-b-15 p-r-15 p-l-15 bg-dark-gray">
                                <h3 class="t-a-c f-w-300 c-white">PESAN</h3>
                                <div id="chat_content"></div>
                            </div>

                            <div id="posisi"></div>
                            <div class="table-responsive m-t-15">
                                <table class="table t-a-c m-r-b">
                                    <tr class="bg-orange c-white">
                                        <td>NO. POSISI</td>
                                        <td>HARGA</td>
                                    </tr>
                                    <tbody id="posisi">
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive">   
                                <table class="table m-r-b">
                                    <tr class="bg-gray c-white">
                                        <td><i>TOTAL STAN</td>
                                        <td class="t-a-r">0</td>
                                    </tr>
                                </table>
                            </div>

                             <div class="table-responsive">   
                                <table class="table m-r-b">
                                    <tr class="bg-light-black c-white">
                                        <td class="b-r-top">
                                            <i>
                                                LAIN LAIN<br/>
                                                DEPOSIT
                                            </i>
                                        </td>
                                        <td class="t-a-r b-r-top">
                                            0<br/>
                                            0
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <hr class="b-orange m-r"/>
                            <div class="table-responsive">   
                                <table class="table m-r-b">
                                    <tr class="bg-light-black c-white">
                                        <td class="b-r-top">
                                            <h6 class="m-r-b">TOTAL TAGIHAN</h6>
                                        </td>
                                        <td class="t-a-r b-r-top">
                                            0
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <hr class="b-orange m-r"/>

                            <div class="table-responsive">   
                                <table class="table m-r-b">
                                    <tr class="bg-light-black c-white">
                                        <td class="b-r-top">
                                            <span class="c-orange t-td-u f-s-12">
                                                <i>detail cicilan</i>
                                            </span>
                                        </td>
                                        <td class="t-a-r b-r-top">
                                            <span class="c-orange f-s-12">
                                                Time remaining: 03:59:59
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="d-flex bd-highlight mb-3">
                                <div class="p-2 bd-highlight">
                                    <button class="btn btn-pesan b-radius-15 b-r bg-green f-s-12 c-white" data-toggle="modal" data-target="#modalconfirmsetuju">
                                        SETUJU
                                    </button>
                                </div>
                                <div class="p-2 bd-highlight">
                                    <button class="btn btn-pesan b-radius-15 b-r bg-red f-s-12 c-white" data-toggle="modal" data-target="#modalconfirmtidaksetuju">
                                        TIDAK
                                    </button>
                                </div>
                                <div class="ml-auto p-2 bd-highlight">
                                    <button class="btn btn-pesan b-radius-15 b-r bg-red f-s-12 c-white" data-toggle="modal" data-target="#modalconfirmbatalpesanan">
                                        BATAL 
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}


    @include('booking.blocks.modal-confirm-booking')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>


    {{-- <script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script> --}}

    {{-- <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase-messaging.js"></script> --}}


    <script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>

    <script src="https://www.gstatic.com/firebasejs/5.7.1/firebase.js"></script>
    
    {{-- <script>
        const headers_1 = {
            "Authorization": "Bearer {{session('my_token___')}}"    
        };

        var config = {
            apiKey: "AIzaSyDdtiewGNa6PYaGwmQ9pxuyO-AiD2Aoevc",
            authDomain: "bukapameran-45e78.firebaseapp.com",
            databaseURL: "https://bukapameran-45e78.firebaseio.com",
            projectId: "bukapameran-45e78",
            storageBucket: "bukapameran-45e78.appspot.com",
            messagingSenderId: "929258258899"
        };

        firebase.initializeApp(config);
        const messaging = firebase.messaging();

        messaging.requestPermission()
            .then(function() {
                console.log("have Permission");
                return messaging.getToken();
            })
            .catch(function(e) {
                console.log("error ", e);
            })

        messaging.getToken().then(function(token){
            // Get Chat Init
            // console.log(token)
            const fd = new FormData()
            fd.append("register_id", token)
            fd.append("authenticator", "browser")
            fd.append("cpyid", {{$id}})

            axios.post("https://tukbase2.bukapameran.com/api/customer/add/nego/chat/init", fd, {headers:headers_1})
                .then(data => {
                    // console.log(data)
                    // console.log(data.data.token.chat_session_token)
                    localStorage.setItem("_chat_token", data.data.token.chat_session_token)
                })
                .catch(e => {
                    console.log(e)
                })
        })

    </script> --}}

    
    <script id="chat_room" type="text/babel" src="{{url('/')}}/assets/master/jsx/chat.jsx?endpoint={{ $pointing->pointing() }}&cpyid={{$id}}&brand_id={{$brand_id}}&acces_token={{session('my_token___')}}&url={{url('/')}}&session_chat={{session('chat_session__')}}"></script>
    {{-- <script id="stan_room" type="text/babel" src="{{url('/')}}/assets/master/jsx/stan.jsx?endpoint={{ $pointing->pointing() }}&cpyid={{$id}}&brand_id={{$brand_id}}&acces_token={{session('my_token___')}}&url={{url('/')}}&session_chat={{session('chat_session__')}}"></script>
    <script id="posisi_room" type="text/babel" src="{{url('/')}}/assets/master/jsx/posisi.jsx?endpoint={{ $pointing->pointing() }}&cpyid={{$id}}&brand_id={{$brand_id}}&acces_token={{session('my_token___')}}&url={{url('/')}}&session_chat={{session('chat_session__')}}"></script> --}}




@endsection
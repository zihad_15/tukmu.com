<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tukmu.com</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    {{-- All Style --}}
    <link rel="stylesheet" href="{{asset('assets/master/css/main.css')}}" />

</head>
<body>

    @yield('content')


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


    <script src="https://www.gstatic.com/firebasejs/5.7.1/firebase.js"></script>
    {{-- <script src="https://www.gstatic.com/firebasejs/5.7.1/firebase.js"></script> --}}
    {{-- <script src="https://www.gstatic.com/firebasejs/5.7.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.7.1/firebase-messaging.js"></script> --}}
     {{-- <script src="{{url('/')}}/firebase_messaging_sw.js"></script> --}}
    {{--<script src="{{url('/')}}/firebase-messaging-sw.js"></script>--}}
    <script>

        // var config = {
        //     apiKey: "AIzaSyDdtiewGNa6PYaGwmQ9pxuyO-AiD2Aoevc",
        //     authDomain: "bukapameran-45e78.firebaseapp.com",
        //     databaseURL: "https://bukapameran-45e78.firebaseio.com",
        //     projectId: "bukapameran-45e78",
        //     storageBucket: "bukapameran-45e78.appspot.com",
        //     messagingSenderId: "929258258899"
        // };

        // firebase.initializeApp(config);
        // const messaging = firebase.messaging();

        // messaging.requestPermission()
        //     .then(function() {
        //         console.log("have Permission");
        //         return messaging.getToken();
        //     })
        //     .catch(function(e) {
        //         console.log("error ", e);
        //     })

        // messaging.getToken().then(function(token){
        //     // Get Chat Init
        //     // console.log(token)
        //     // const fd = new FormData()
        //     // fd.append("register_id", token)
        //     // fd.append("authenticator", "browser")
        //     // fd.append("cpyid", "CPY201812220000000005")

        //     // axios.post("https://tukbase2.bukapameran.com/api/customer/add/nego/chat/init", fd, {headers:headers_})
        //     //     .then(data => {
        //     //         console.log(data)
        //     //         console.log(data.data.token.chat_session_token)
        //     //         localStorage.setItem("_chat_token", data.data.token.chat_session_token)
        //     //     })
        //     //     .catch(e => {
        //     //         console.log(e)
        //     //     })
        // })

    </script>


</body>
</html>
@extends('layouts.master')
@section('st_css')
	<style type="text/css">
		.jumbotron {
			padding: 1%;
		}
	</style>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
@stop
@section('content')
  @include('booking.blocks.header')
  <div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">
      <br>
      <br>
      <h2>DAFTAR BARANG</h2>
      <p><i>Lampirkan daftar barang yang akan Anda bawa<br>Untuk dibuat kedalam surat masuk event</i></p>
      <div class="jumbotron jumbotron-fluid">
        <div class="container">
          <label>Surat</label>
          <select class="form-control" id="sel1" name="sellist1" style="width: 20%;">
            <option value="1">Masuk</option>
            <option value="2">Keluar</option>
          </select>
          <br>
          <div class="row" class="form-group">
            <div class="col-md-3">
              <label>Penanggung Jawab</label>
              <input class="form-control" type="text" id="" name="">
            </div>
            <div class="col-md-3">
              <label>Telepon</label>
              <input class="form-control" type="number" id="" name="">
            </div>
            <div class="col-md-3">
              <label>Tanggal</label>
              <input class="form-control" type="date" id="" name="">
            </div>
          </div>
          <br>
          <div class="row" id="dynamic_field" class="form-group">
            <div class="col-md-6">
              <label>Nama Barang</label>
                <input type="text" name="name[]" class="form-control name_list" />
              </table>
            </div>
            <div class="col-md-2">
              <label>Jumlah</label>
              <input class="form-control" type="number" id="" name="">
            </div>
            <div class="col-md-2">
              <label></label>
              <button class="btn btn-primary form-control" name="add" id="add" style="margin-top: 7px; background-color: darkorange; border-color: darkorange;">Tambahkan</button>
            </div>
          </div>
          <br>
          <div class="row" class="form-group">
            <div class="col-md-3">
              <button class="btn btn-primary" style="margin-top: 7px; background-color: darkorange; border-color: darkorange;">SELESAI</button>
            </div>
            <div class="col-md-6">
              <div class="jumbotron" style="background-color: white;">
                <div class="row">
                  <div class="col-md-1">
                    <img src="assets/master/img/tandaseru.png" width="40" height="40">
                  </div>
                  <div class="col-md-10" style="margin-bottom: -15px;">
                    <p style="font-size: 13px; margin-left: 3%"><strong><i>PASTIKAN DAFTAR BARANG YANG ANDA AJUKAN SESUAI DENGAN <span><a href="" style="color: darkorange;">SYARAT & KETENTUAN</a></span></i></strong></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-2">
    </div>
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      var postURL = "<?php echo url('addmore'); ?>";
      var i=1;


      $('#add').click(function(){
           i++;
           $('#dynamic_field').append('<div class="col-md-12 dynamic-added" id="row'+i+'"><div class="row" style="margin-top: 1%;"><div class="col-md-6"><input type="text" name="name[]" class="form-control name_list" /></div><div class="col-md-2"><input class="form-control" type="number" id="" name=""></div><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></div>');
      });


      $(document).on('click', '.btn_remove', function(){
           var button_id = $(this).attr("id");
           $('#row'+button_id+'').remove();
      });


      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


      $('#submit').click(function(){
           $.ajax({
                url:postURL,
                method:"POST",
                data:$('#add_name').serialize(),
                type:'json',
                success:function(data)
                {
                    if(data.error){
                        printErrorMsg(data.error);
                    }else{
                        i=1;
                        $('.dynamic-added').remove();
                        $('#add_name')[0].reset();
                        $(".print-success-msg").find("ul").html('');
                        $(".print-success-msg").css('display','block');
                        $(".print-error-msg").css('display','none');
                        $(".print-success-msg").find("ul").append('<li>Record Inserted Successfully.</li>');
                    }
                }
           });
      });


      function printErrorMsg (msg) {
         $(".print-error-msg").find("ul").html('');
         $(".print-error-msg").css('display','block');
         $(".print-success-msg").css('display','none');
         $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
         });
      }
    });
</script>
@endsection

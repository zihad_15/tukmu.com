@extends('layouts.master')

@section('content')
	<div class="container">
		<h1 style="text-align: center; display: block; margin: auto; margin-top: 20%;">REGISTRASI BERHASIL! <br> SILAHKAN CEK EMAIL UNTUK PROSES VERIFIKASI.</h1>
	</div>

	<script type="text/javascript">
		setTimeout(function () {
		   window.location.href = "{{ url('/') }}"; //will redirect to your blog page (an ex: blog.html)
		}, 5000); //will call the function after 2 secs.
	</script>
@stop
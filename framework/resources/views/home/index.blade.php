@extends('layouts.master')
@section('content')
    
    @include('booking.blocks.header')

@if (session('error_code') == 5)
    <script>
    // function myFunction() {
       alert("Mohon untuk melakukan login terlebih dahulu!");
    // }
    // window.onload = myFunction;
    </script>
@endif
@if (session('error_code') == 1)
    <script>
    // function myFunction() {
       alert("Email atau Password salah! pastikan Email atau Password anda benar dan coba lagi.");
    // }
    // window.onload = myFunction;
    </script>
@endif
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="assets/master/img/home/slider/001.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/master/img/home/slider/002.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/master/img/home/slider/003.jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<br>
<br>

<div class="container">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<div class="jumbotron">
				<div class="row">
				  	<div class="col-md-5">
				  		<h6>CARI</h6>
				  		<input type="text" class="form-control" name="search_params" id="search_params">
				  	</div>
				  	<div class="col-md-2">
				  		<h6 style="font-size: 14px;">TANGGAL MULAI</h6>
				  		<input class="form-control" type="date" name="start_date" id="start_date">
				  	</div>
                    <div class="col-md-2">
                        <h6 style="font-size: 14px;">TANGGAL SELESAI</h6>
                        <input class="form-control" type="date" name="end_date" id="end_date">
                    </div>
				  	<div class="col-md-3">
				  		<h6 style="font-size: 14px;">KATEGORI EVENT</h6>
                        <select class="form-control" name="category__" id="category__">
                            <option value="0">Pilih Kategori</option>
                            @foreach ($category->category_list as $key => $value)
                            <option value="{{$value->eventcatid}}">{{$value->eventcatname}}</option>
                            @endforeach
                        </select>
				  	</div>
				</div>
				<br><br>
				<div class="row">
				  	<div class="col-md-5"></div><!--  -->
				  	<div class="col-md-2">
				  		<button type="button" id="cari_event" class="btn btn-defaullt" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white"><b>CARI
                        &nbsp;<i id="loading-cari" class="fas fa-spinner fa-spin d-none"></i></b></span></button>
				  	</div>
				  	<div class="col-md-5"></div>
				</div>
			</div>
		</div>
		<div class="col-md-1"></div>
    </div>
        <div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="row">
					<button id="fixtime" class="btn btn-defaullt d-none" style="margin-right: 20px; margin-left: 15px; color: white; background-color: rgb(143,143,143);">FIX TIME</button>
					<button id="flexibletime" class="btn btn-defaullt d-none" style="background-color: #ff9649; color: white;">FLEXIBLE TIME</button>
				</div>
				<br>
				<h4 id="hasil_pencarian" class="d-none">HASIL PENCARIAN</h4>
				<br>
                <div class="jumbotron d-none" id="result_div" style="padding: 10px;">
                    <div id="result_search">
                        
                    </div>
				</div>
                <div id="pagination" class="d-none" style="margin: auto;display:block;width: 300px;max-width:350px; overflow-x:auto">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            {{-- <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li> --}}
                        </ul>
                    </nav>
                </div>
			</div>
			<div class="col-md-2"></div>
		</div>
</div>

<br>

<div class="container" style="margin-top: -30px;">

	<center>
		<h1>UPCOMING EVENT</h1>
	</center>
    <hr style="background-color: orange;">
	<br>
    <div class="row blog">
        <div class="col-md-12">
            <div id="blogCarousel" class="carousel slide" data-ride="carousel">

                <ol class="carousel-indicators">
                    <?php 
                        if($upcomingevent->promoted_event > 4) {
                            $event = 2;
                        } else {
                            $event = 1;
                        }
                         ?>
                    @for($i = 0; $i < $event; $i++)
                    <li data-target="#blogCarousel" data-slide-to="{{$i}}" @if($i == 0)class="active"@endif></li>
                    @endfor
                </ol>

                <!-- Carousel items -->
                <div class="carousel-inner">
                    @for($i = 0; $i < $event; $i++)
                        <div @if($i == 0)class="carousel-item active" @else class="carousel-item" @endif>          
                            <div class="row">
                                @foreach($upcomingevent->promoted_event as $key => $value)
                                @if($i == 0 && $key <= 3)
                                <div class="col-md-3">
                                    <div class="jumbotron" style="padding: 15px; height: 100%;">
                                        <center>
                                            @if(isset($value->first_media->spacemediapath))
                                            <img src="https://tukbase2.bukapameran.com/picGet/{{ $value->first_media->spacemediapath }}" width="200" height="150">
                                            @endif
                                            <br>
                                            <br>
                                            @if(isset($value->eventname))
                                            <h5>{{ $value->eventname }}</h5>
                                            @endif
                                            @if(isset($value->full_address))
                                            <p style="font-size: 14px; line-height: 130%;">{{ $value->full_address }}</p>
                                            @endif
                                            <br>
                                            <a href="{{ url('detail-event/'.$value->spaceid) }}" class="btn btn-default btn-sm" style="background-color: #f47b50; color: white; margin-top: -30%;">Selengkapnya</a>
                                        </center>
                                    </div>
                                </div>
                                @elseif($i == 1 && $key > 3)
                                <div class="col-md-3">
                                    <div class="jumbotron" style="padding: 15px; height: 100%;">
                                        <center>
                                            @if(isset($value->first_media->spacemediapath))
                                            <img src="https://tukbase2.bukapameran.com/picGet/{{ $value->first_media->spacemediapath }}" width="200" height="150">
                                            @endif
                                            <br>
                                            <br>
                                            @if(isset($value->eventname))
                                            <h5>{{ $value->eventname }}</h5>
                                            @endif
                                            @if(isset($value->full_address))
                                            <p style="font-size: 14px; line-height: 130%;">{{ $value->full_address }}</p>
                                            @endif
                                            <br>
                                            <a href="{{ url('detail-event/'.$value->spaceid) }}" class="btn btn-default btn-sm" style="background-color: #f47b50; color: white; margin-top: -30%;">Selengkapnya</a>
                                        </center>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div>
                            <!--.row-->
                        </div>
                    @endfor
                    <!--.item-->

                    <!-- <div class="carousel-item">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#">
                                    <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">
                                    <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">
                                    <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">
                                    <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                </a>
                            </div>
                        </div>
                    </div> -->

                </div>
                <!--.carousel-inner-->
            </div>
            <!--.Carousel-->

        </div>
    </div>
</div>
@endsection

@section('footer')
<footer class="footer">
    <div class="container bottom_border">
        <div class="row">
            <div class=" col-sm-3 col-md col-sm-4  col-12 col">
                <h3 class="headin5_amrc col_white_amrc pt2">About Us</h3>
<!--headin5_amrc-->
                <p class="mb10" style="color: black;">With more than 15 years of experience we can proudly say that we are one of the best in business, a trusted supplier for more than 1000 companies.</p>


            </div>


            <div class=" col-sm-3 col-md  col-6 col">
                <h3 class="headin5_amrc col_white_amrc pt2">Site Map</h3>
                <!--headin5_amrc-->
                <ul class="footer_ul_amrc">
                <li><a href="#" style="color: black;">About</a></li>
                <li><a href="#" style="color: black;">Bantuan</a></li>
                <li><a href="#" style="color: black;">Daftar</a></li>
                <li><a href="#" style="color: black;">Masuk</a></li>
                </ul>
            <!--footer_ul_amrc ends here-->
            </div>

            <div class=" col-sm-3 col-md  col-6 col">
                <h3 class="headin5_amrc col_white_amrc pt2">Get In Touch</h3>

                    <a href="#"><i class="fab fa-facebook-f" style="margin-right: 15px;"></i></a>
                    <a href="#"><i class="fab fa-twitter" style="margin-right: 15px;"></i></a>
                    <a href="#"><i class="fab fa-linkedin" style="margin-right: 15px;"></i></a>
                    <a href="#"><i class="fab fa-instagram" style="margin-right: 15px;"></i></a>
            </div>

            <div class=" col-sm-3 col-md  col-12 col">
                <h3 class="headin5_amrc col_white_amrc pt2">Newsletter</h3>
                <p style="color: black;">Subscribe us for new update information of our event</p>
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subscribe" placeholder="Enter your email here">
                    </div>
                    <button type="submit" class="btn btn-default btn-sm" style="background-color: #f47b50; color: white;">SUBSCRIBE</button>
                </form>
            </div>
        </div>
    </div>


    <div class="container">
        <!--foote_bottom_ul_amrc ends here-->
        <p class="text-center" style="color: black;">Copyright @2017 | Designed With by <a href="#">tukmu.com</a></p>

        
        <!--social_footer_ul ends here-->
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection
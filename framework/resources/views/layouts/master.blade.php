<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta content="Web Service" name="description" />
	<meta content="Tukmu Web Service" name="author" />
	<link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
	<title>@yield('title', 'Home - Tukmu.com')</title>



	<!-- GLOBAL CSS & FONT -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/master/css/bootstrap.css') !!}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- PARTIAL CSS -->
    @yield('rp_css')
    @yield('up_css')
    @yield('ep_css')
    @yield('st_css')
    @yield('dt_css')
    @yield('pm_css')

    <style>
    .row.list-content {
        border-bottom: 10px solid #fff;
        padding-bottom: 10px
    }
    .result_search .row:last-child {
        border-bottom: 0px !important;
    }
    </style>


</head>
<body>


    @yield('content')

    <br>
    <br>

    @yield('footer')
    <!-- GLOBAL JAVASCRIPT & JQUERY -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>

    <script src="https://www.gstatic.com/firebasejs/5.7.1/firebase.js"></script>
    @if(Request::segment(1) != 'detail-event')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    {{-- <script src="https://www.gstatic.com/firebasejs/5.7.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.7.1/firebase-messaging.js"></script> --}}
    {{-- <script src="{{url('/')}}/firebase_messaging_sw.js"></script> --}}

    <script>

        // var config = {
        //     apiKey: "AIzaSyDdtiewGNa6PYaGwmQ9pxuyO-AiD2Aoevc",
        //     authDomain: "bukapameran-45e78.firebaseapp.com",
        //     databaseURL: "https://bukapameran-45e78.firebaseio.com",
        //     projectId: "bukapameran-45e78",
        //     storageBucket: "bukapameran-45e78.appspot.com",
        //     messagingSenderId: "929258258899"
        // };

        // firebase.initializeApp(config);
        // const messaging = firebase.messaging();

        // messaging.requestPermission()
        //     .then(function() {
        //         console.log("have Permission");
        //         return messaging.getToken();
        //     })
        //     .catch(function(e) {
        //         console.log("error ", e);
        //     })

        // messaging.getToken().then(function(token){
        //     // Get Chat Init
        //     console.log(token)
        //     // const fd = new FormData()
        //     // fd.append("register_id", token)
        //     // fd.append("authenticator", "browser")
        //     // fd.append("cpyid", "CPY201812220000000005")

        //     // axios.post("https://tukbase2.bukapameran.com/api/customer/add/nego/chat/init", fd, {headers:headers_})
        //     //     .then(data => {
        //     //         console.log(data)
        //     //         console.log(data.data.token.chat_session_token)
        //     //         localStorage.setItem("_chat_token", data.data.token.chat_session_token)
        //     //     })
        //     //     .catch(e => {
        //     //         console.log(e)
        //     //     })
        // })
    </script>

    <!-- PARTIAL JAVASCRIPT & JQUERY -->
    @yield('rp_js')
    @yield('pm_js')

    <!-- CEK USER EMAIL -->
    <script>
         $('#email_peng').change(function () {
            var email_peng = $('#email_peng').val();
            var cekemail = {
                "async": true,
                "crossDomain": true,
                "url": "https://tukbase2.bukapameran.com/api/customer/exists?email="+email_peng,
                "method": "GET"
        }
             // console.log((cekemail));

                $.ajax(cekemail).done(function (response) {
                    //console.log(response)
                    if (response.status === "1") {
                        $("#emailnotverif").hide();
                    } else {
                        $("#emailnotverif").show();
                    }
                })
        });
    </script>

    <script>
        $('document').ready(function(){
            var base_url = '{{URL::to('/')}}';
            // var querystring = require('querystring');
            var resultlimit = 0
            var page = 1;
            var search_params = $('#search_params').val();
            var end_date = $('#end_date').val();
            var start_date = $('#start_date').val();
            var category = $('#category__').val();
            $('#daftar').click(function(){
                // var nama = $('#name').val();
                // var email = $('#email').val();
                // var password = $('#password').val();

                // $("#loading-dafftar").removeClass("d-none")
                // $("#daftar").addClass("disabled")

                // var axiosConfig = {
                //     headers: {
                //         'Content-Type': 'application/x-www-form-urlencoded',
                //         "Access-Control-Allow-Origin": "*",
                //     }
                // };

                // var requestBody = {
                //     "pic_name": nama,
                //     "pic_email": email,
                //     "pic_telp": 0,
                //     "pic_password": password
                // };

                // axios.post('/api/registrasi', {
                //     "pic_name": nama,
                //     "pic_email": email,
                //     "pic_telp": 0,
                //     "pic_password": password
                // }, axiosConfig)
                // .then(function (response) {
                //     // if(response.status === "1"){
                //     //     $('#sukses-daftar').removeClass('d-none')
                //     //     setTimeout(function(){
                //     //         $('#sukses-daftar').addClass('d-none')
                //     //     }, 5000)
                //     //     $("#loading-dafftar").addClass("d-none")
                //     // } else {
                //     //     console.log("Gagal")
                //     //     $("#loading-dafftar").addClass("d-none")
                //     // }
                //     console.log(response)
                //     $("#loading-dafftar").addClass("d-none")
                // })
                // .catch(function (error) {
                //     console.log(error);
                //     $("#loading-dafftar").addClass("d-none")
                // });

                // var form = new FormData();
                // form.append("pic_email", email);
                // form.append("pic_password", password);
                // form.append("pic_name", nama);
                // form.append("pic_telp", 9999999);

                // var settings = {
                // "async": true,
                // "crossDomain": true,
                // "url": "api/registrasi",
                // "method": "POST",
                // "headers": {
                //     "Access-Control-Allow-Origin": "*"
                // },
                // "processData": false,
                // "contentType": false,
                // "mimeType": "multipart/form-data",
                // "data": form
                // }

                // $.ajax(settings).done(function (response) {
                //     if(response.status === "1"){
                //         $('#sukses-daftar').removeClass('d-none')
                //         setTimeout(function(){
                //             $('#sukses-daftar').addClass('d-none')
                //         }, 5000)
                //         $("#loading-dafftar").addClass("d-none")
                //         $("#daftar").removeClass("disabled")
                //     } else {
                //         console.log("Gagal")
                //         $("#loading-dafftar").addClass("d-none")
                //         $("#daftar").removeClass("disabled")
                //     }
                //     console.log(response)
                // });

            });

            pageNextOrPrev = (pagenumb) => {
                $('html, body').animate({
                    scrollTop: $("#result_search").offset().top
                }, 2000);
                $('#result_search').empty();
                $('#pagination  .pagination').empty();
                resultlimit = 0;
                loadData(pagenumb);
            }

            function changeResultLimit(data) {
                resultlimit = data;
            }

            loadData = (pagenumb) => {
                // console.log(resultlimit)
                $(".result_search .row:last-child").css({'border':'0px'})
                page = pagenumb;
                console.log(page)
                var url = "";
                if(category != "0"){
                    url = "https://tukbase2.bukapameran.com/api/event/search?page="+page+"&search_param="+search_params+"&category[0]="+category+"&search_date="+start_date+"&finish_date="+end_date;
                } else {
                    url = "https://tukbase2.bukapameran.com/api/event/search?page="+page+"&search_param="+search_params+"&search_date="+start_date+"&finish_date="+end_date;
                }
                // console.log(url)

                // console.log(category)
                var settings = {
                    "async": false,
                    "crossDomain": true,
                    "url": url,
                    "method": "GET"
                }

                if(resultlimit === 0){
                    $.ajax(settings).done((response) => {
                        if(response.status === "1"){
                            changeResultLimit(1)
                            // setTimeout(function(){
                            //     $('#sukses-daftar').addClass('d-none')
                            // }, 5000)
                            // console.log(settings)
                            // console.log(response);
                            var content = "";
                            var pagination = "";

                            if(response.total_page > 1){
                                $("#pagination").removeClass("d-none")
                                for(var i = 1; i <= response.total_page; i++){
                                    var pageactive = '';
                                    if(page == i){
                                        pageactive = 'active';
                                    } else {
                                        pageactive = '';
                                    }
                                    pagination +=
                                        '<li class="page-item '+pageactive+'" onclick="pageNextOrPrev('+i+')">'+
                                            '<a class="page-link" href="javascript:;">'+
                                                ''+i+''+
                                            '</a>'+
                                        '</li>';
                                }
                                $('#pagination .pagination').append(pagination)
                            }

                            response.search_value.forEach((value, index) => {
                                var img_logo = '';
                                var pemisah = '';
                                var arrstartdate = value.startdate.split('-');
                                var arrfinishdate = value.finishdate.split('-');
                                var newstartdate = arrstartdate[1] + '-' + arrstartdate[2] + '-' + arrstartdate[0];
                                var newfinishdate = arrfinishdate[1] + '-' + arrfinishdate[2] + '-' + arrfinishdate[0];

                                if(value.first_media != null && value.first_media !== null){
                                    img_logo += '<img src="https://tukbase2.bukapameran.com/picGet/'+value.first_media.spacemediapath+'" width="170" height="170" style="margin-top: 30px;">';
                                } else {
                                    img_logo += '<img src="assets/master/img/search-engine/sample.png" width="170" height="170" style="margin-top: 30px;">';
                                }

                                // if(index+1 === response.search_value.length){
                                //     pemisah = '';
                                // } else {
                                //     pemisah =
                                //     '<div class="row br">'+
                                //         '<br/>'+
                                //     '</div>';
                                // }

                                content +=
                                    '<div class="row list-content">'+
                                        '<div class="col-md-3">'+
                                            img_logo+
                                        '</div>'+
                                        '<div class="col-md-5">'+
                                            '<p style="color: red;"><i>Highlight</i></p>'+
                                            '<h5>'+value.eventname+'</h5>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; '+newstartdate+' - '+newfinishdate+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; '+value.spacename+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/rp.png" width="20" height="20">&nbsp; Rp. '+value.minprice+' - Rp. '+value.maxprice+
                                            '</p>'+
                                            '<span>PENGELOLA / EVENT ORGANIZER</span>'+
                                            '<br>'+
                                            '<span><a href="#" style="color: #ff9649">'+value.space_provider.spaceprovname+'</a></span>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<a href="'+base_url+'/detail-event/'+value.spaceid+'" class="btn btn-defaullt btn-sm" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white">DETAIL EVENT</span></a>'+
                                        '</div>'+
                                    '</div>'+
                                    // ''+pemisah+''+
                                    '<br/>';
                            });

                            $('#result_search').append(content);
                        } else {
                            console.log("Gagal")
                            $("#loading-cari").addClass("d-none")
                            $("#fixtime").addClass("d-none")
                            $("#flexibletime").addClass("d-none")
                            $("#hasil_pencarian").addClass("d-none")
                            $("#result_div").addClass("d-none")
                            changeResultLimit(1)
                        }
                        // console.log(response)
                    });
                    // console.log(resultlimit)
                }
                if(resultlimit === 1) {
                    resultlimit = 0;
                    $("#loading-cari").addClass("d-none")
                    $("#fixtime").removeClass("d-none")
                    $("#flexibletime").removeClass("d-none")
                    $("#hasil_pencarian").removeClass("d-none")
                    $("#result_div").removeClass("d-none")
                }
            }

            $('#cari_event').click(function(e){

                $("#loading-cari").removeClass("d-none")
                $("#fixtime").addClass("d-none")
                $("#flexibletime").addClass("d-none")
                $("#hasil_pencarian").addClass("d-none")
                $("#result_div").addClass("d-none")
                $('#result_search').empty()

                resultlimit = 0;
                // e.preventDefault();
                loadData(page)
                // $( this ).off( e );
            });


            $('#fixtime').on('click', function(){

                $('#result_search').empty()

                // $("#fixtime").addClass("d-none")
                // $("#flexibletime").addClass("d-none")
                $("#hasil_pencarian").addClass("d-none")
                $("#result_div").addClass("d-none")
                $('#result_search').empty()

                var url = "";
                if(category != "0"){
                    url = "https://tukbase2.bukapameran.com/api/event/search?page="+page+"&search_param="+search_params+"&category[0]="+category+"&search_date="+start_date+"&finish_date="+end_date;
                } else {
                    url = "https://tukbase2.bukapameran.com/api/event/search?page="+page+"&search_param="+search_params+"&search_date="+start_date+"&finish_date="+end_date;
                }

                // console.log(category)
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": url,
                    "method": "GET"
                }

                $.ajax(settings).done(function (response) {
                    if(response.status === "1"){
                        // setTimeout(function(){
                        //     $('#sukses-daftar').addClass('d-none')
                        // }, 5000)
                        // console.log(settings)
                        // $("#fixtime").removeClass("d-none")
                        // $("#flexibletime").removeClass("d-none")
                        $("#hasil_pencarian").removeClass("d-none")
                        $("#result_div").removeClass("d-none")
                        // console.log(response);
                        var content = "";
                        var data_content = [];
                        var i = 1;
                        response.search_value.forEach((value, index) => {
                            var img_logo = '';
                            var pemisah = '';

                            if(value.opendateflag === 0){

                                if(value.first_media != null && value.first_media !== null){
                                    img_logo += '<img src="https://tukbase2.bukapameran.com/picGet/'+value.first_media.spacemediapath+'" width="170" height="170" style="margin-top: 30px;">';
                                } else {
                                    img_logo += '<img src="assets/master/img/search-engine/sample.png" width="170" height="170" style="margin-top: 30px;">';
                                }

                                // if(i !== data_content.length){
                                //     pemisah =
                                //     '<div class="row br">'+
                                //         '<br/>'+
                                //     '</div>';
                                // } else {
                                //     pemisah = '';
                                // }

                                // console.log(data_content.length)
                                // console.log(i)

                                content +=
                                    '<div class="row list-content">'+
                                        '<div class="col-md-3">'+
                                            img_logo+
                                        '</div>'+
                                        '<div class="col-md-5">'+
                                            '<p style="color: red;"><i>Highlight</i></p>'+
                                            '<h5>INDEPENDENCE DAY</h5>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; '+value.startdate+' - '+value.finishdate+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; '+value.spacename+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/rp.png" width="20" height="20">&nbsp; Rp. '+value.minprice+' - Rp. '+value.maxprice+
                                            '</p>'+
                                            '<span>PENGELOLA / EVENT ORGANIZER</span>'+
                                            '<br>'+
                                            '<span><a href="#" style="color: #ff9649">'+value.space_provider.spaceprovname+'</a></span>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<a href="'+base_url+'/detail-event/'+value.spaceid+'" class="btn btn-defaullt btn-sm" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white">DETAIL EVENT</span></a>'+
                                        '</div>'+
                                    '</div>'+
                                    // ''+pemisah+''+
                                    '<br/>';
                                i ++;
                                data_content.push(value.opendateflag)

                            }

                        });

                        if(data_content.length !== 0){
                            $('#result_search').append(content)
                        } else {
                            $('#result_search').append('<p style="text-align: center; color: red">Event tidak ditemukan!<p>')
                        }
                    } else {
                        console.log("Gagal")
                        $("#loading-cari").addClass("d-none")
                        // $("#fixtime").addClass("d-none")
                        // $("#flexibletime").addClass("d-none")
                        $("#hasil_pencarian").addClass("d-none")
                        $("#result_div").addClass("d-none")
                    }
                    // console.log(response)
                });
            });

            $('#flexibletime').on('click', function(){

                $('#result_search').empty()

                // $("#fixtime").addClass("d-none")
                // $("#flexibletime").addClass("d-none")
                $("#hasil_pencarian").addClass("d-none")
                $("#result_div").addClass("d-none")
                $('#result_search').empty()

                var url = "";
                if(category != "0"){
                    url = "https://tukbase2.bukapameran.com/api/event/search?page="+page+"&search_param="+search_params+"&category[0]="+category+"&search_date="+start_date+"&finish_date="+end_date;
                } else {
                    url = "https://tukbase2.bukapameran.com/api/event/search?page="+page+"&search_param="+search_params+"&search_date="+start_date+"&finish_date="+end_date;
                }

                // console.log(category)
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": url,
                    "method": "GET"
                }

                $.ajax(settings).done(function (response) {
                    if(response.status === "1"){
                        // setTimeout(function(){
                        //     $('#sukses-daftar').addClass('d-none')
                        // }, 5000)
                        // console.log(settings)
                        // $("#fixtime").removeClass("d-none")
                        // $("#flexibletime").removeClass("d-none")
                        $("#hasil_pencarian").removeClass("d-none")
                        $("#result_div").removeClass("d-none")
                        // console.log(response);
                        var content = "";
                        var data_content = [];
                        response.search_value.forEach((value, index) => {
                            var img_logo = '';
                            var pemisah = '';

                            if(value.opendateflag === 1){

                                if(value.first_media != null && value.first_media !== null){
                                    img_logo += '<img src="https://tukbase2.bukapameran.com/picGet/'+value.first_media.spacemediapath+'" width="170" height="170" style="margin-top: 30px;">';
                                } else {
                                    img_logo += '<img src="assets/master/img/search-engine/sample.png" width="170" height="170" style="margin-top: 30px;">';
                                }

                                // if(index+1 === response.search_value.length){
                                //     pemisah = '';
                                // } else {
                                //     pemisah =
                                //     '<div class="row br">'+
                                //         '<br/>'+
                                //     '</div>';
                                // }

                                content +=
                                    '<div class="row list-content">'+
                                        '<div class="col-md-3">'+
                                            img_logo+
                                        '</div>'+
                                        '<div class="col-md-5">'+
                                            '<p style="color: red;"><i>Highlight</i></p>'+
                                            '<h5>INDEPENDENCE DAY</h5>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; '+value.startdate+' - '+value.finishdate+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; '+value.spacename+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/rp.png" width="20" height="20">&nbsp; Rp. '+value.minprice+' - Rp. '+value.maxprice+
                                            '</p>'+
                                            '<span>PENGELOLA / EVENT ORGANIZER</span>'+
                                            '<br>'+
                                            '<span><a href="#" style="color: #ff9649">'+value.space_provider.spaceprovname+'</a></span>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<a href="'+base_url+'/detail-event/'+value.spaceid+'" class="btn btn-defaullt btn-sm" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white">DETAIL EVENT</span></a>'+
                                        '</div>'+
                                    '</div>'+
                                    // ''+pemisah+''+
                                    '<br/>';
                                data_content.push(value.opendateflag)
                            }

                        });

                        if(data_content.length !== 0){
                            $('#result_search').append(content)
                        } else {
                            $('#result_search').append('<p style="text-align: center; color: red">Event tidak ditemukan!<p>')
                        }

                    } else {
                        console.log("Gagal")
                        $("#loading-cari").addClass("d-none")
                        // $("#fixtime").addClass("d-none")
                        // $("#flexibletime").addClass("d-none")
                        $("#hasil_pencarian").addClass("d-none")
                        $("#result_div").addClass("d-none")
                    }
                    // console.log(response)
                });
            });
        });
    </script>
    @endif


</body>
</html>

@extends('layouts.master')
@section('pm_css')
	
	@include('booking.blocks.header')

	<style type="text/css">
		.jumbotron{
			padding-top: 20px;
			padding-left: 25px;		
		}

		.hide{
			display: none;
		}

		.imageContainer {
	       background-image: url('{!! asset('assets/master/img/bayar.png') !!}');
	       background-repeat: no-repeat;
	       background-size: 150px;
 		}
 		
 		#time-count-down {
          margin-bottom: 15px;
        }
        #time-count-down .p-2 {
          background: orange;
          margin-right: 1px;
          color: #fff;
          font-size: 30px;
          padding: 0px 7px !important;
          font-weight: 700;
        }
        #time-count-down .p-2.titik-dua {
          background: transparent;
          color: orange;
        }
        .lokasi-list {
            display: flex;
            padding: 0px;
            margin: 0px;
        }
        .lokasi-list li {
            list-style: none;
            text-decoration: none;
            color: darkorange;
            margin-right: 5px;
        }
	</style>
@stop
@section('content')
	<div class="container" style="margin-top: 70px; margin-bottom: 20px;">
		<div>
			<h3>TERIMA KASIH!</h3>
			<p>Pesanan Anda Telah Kami Terima</p>
		</div>
		<form action="pembayaranProses" method="POST">
			@csrf
			<input type="text" id="cpid" name="cpid" value="{{ $paymentsummary->customer_payment_deposit->custpaymentid }}" hidden>
			<div class="row">
				<div class="col-md-8">
					<div class="jumbotron jumbotron-fluid">
					  <div class="container">
					    <input type="radio" name="tab" id="bank-tab" onclick="tab1()"><span>&nbsp; <b>TRANSFER BANK</b></span>
					    <hr style="background-color: #ff9649;">
						<div id="div1" class="hide">
						  	<p>Pilih bank partner</p>
						  	<table>
						  		<tbody style="text-align: center;">
						  			<tr>
						  				<td><img src="{!! asset('assets/master/img/bca.png') !!}" width="100px"></td>
						  				<td>&nbsp; &nbsp; &nbsp;</td>
					  					<td><img src="{!! asset('assets/master/img/mandiri.png') !!}" width="100px"></td>
					  					<td>&nbsp; &nbsp; &nbsp;</td>
					  					<td>IMAGE</td>
						  			</tr>
						  			<tr>
						  				<td><h6>BCA</h6></td>
						  				<td>&nbsp; &nbsp; &nbsp;</td>
						  				<td><h6>MANDIRI</h6></td>
						  				<td>&nbsp; &nbsp; &nbsp;</td>
						  				<td><h6>BANK LAIN</h6></td>
						  			</tr>
						  			<tr>
						  				<td><input type="radio" name="tab2" id="tab2" value="10" onclick="tab3()"></td>
						  				<td>&nbsp; &nbsp; &nbsp;</td>
						  				<td><input type="radio" name="tab2" id="tab2" value="11"></td>
						  				<td>&nbsp; &nbsp; &nbsp;</td>
						  				<td><input type="radio" name="tab2" id="tab2" value="20"></td>
						  			</tr>
						  		</tbody>
						  	</table>
						  	<hr style="background-color: orange;">
						</div>

						<div id="div2" class="hide">
							<nav>
							  <div class="nav nav-tabs" id="nav-tab" role="tablist">
							    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><img src="{!! asset('assets/master/img/kartu.png') !!}" style="margin-bottom: 4px;" width="35px">
							    	<br>
							    <b style="color: #ff9649;">ATM</b></a>
							    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"><img src="{!! asset('assets/master/img/www.png') !!}" style="margin-left: 20px;" width="35px">
							    	<br>
							    <b style="color: #ff9649;">E-Banking</b></a>
							    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"><img src="{!! asset('assets/master/img/hp.png') !!}" style="margin-left: 40px;" width="35px">
							    	<br>
							    <b style="color: #ff9649;">Mobile Payment</b></a>
							  </div>
							</nav>
							<div class="tab-content" id="nav-tabContent" style="background-color: white; padding: 25px 25px 25px 25px;">
							  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
							  	<ul>
							  		<li>Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA, dan Bank Mandiri</li>
							  		<li>Total belanja kamu belom termasuk kode pembayaran untuk keperluan proses verifikasi otomatis</li>
							  		<li>Mohon transfer sampai tepat 3 digit trakhir</li>
							  	</ul>
							  </div>
							  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">...</div>
							  <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...</div>
							</div>
						</div>

						<br>
					    <input type="radio" name="tab" id="debit-tab" onclick="tab2()"><span>&nbsp; <b>KARTU KREDIT</b></span>
					    <hr style="background-color: #ff9649;">
					  </div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="jumbotron jumbotron-fluid">
					  <div class="container" style="font-weight: bold;">
					    <h5>INDEPENDENCE DAY</h5>
					    <p class="f-p-s"><img src="{!! asset('assets/master/img/search-engine/date.png') !!}" width="15" height="17">&nbsp; {{ date("d-M-Y", strtotime($paymentsummary->event_data->startdate)) }} - {{ date("d-M-Y", strtotime($paymentsummary->event_data->finishdate)) }}</p>
			    		<p class="f-p-s"><img src="{!! asset('assets/master/img/search-engine/location.png') !!}" width="15" height="17">&nbsp; {{ $paymentsummary->event_data->spacename }}</p>
			    		<p>PENGELOLA / EVENT ORGINIZER <a href="#" style="color: #ff9649;"><br>{{ $paymentsummary->event_data->space_provider->spaceprovname }}</a></p>
			    		<p>LOKASI</p>
			    		    <ul class="lokasi-list">
			    			<?php
				    			$length = count($paymentsummary->payment_data->payment_grid);
								for ($i = 0; $i < $length; $i++) {
									echo "<li>";
									if($length == $i){
								  	    echo $paymentsummary->payment_data->payment_grid[$i]->code;
									} else {
								  	    echo $paymentsummary->payment_data->payment_grid[$i]->code.",";
									}
								  	echo "</li>";
								} 
							?>
			    		</ul>
					  </div>
					  <div class="container">
					  	<hr style="background-color: #ff9649;">
					  	<h5 style="color: #ff9649; margin-top: -14px; margin-bottom: -10px;"><img src="{!! asset('assets/master/img/search-engine/rp.png') !!}" width="25" height="25">&nbsp; <b>{{ $paymentsummary->grand_total_payment }}</b></h5>
					  	<hr style="background-color: #ff9649;">
					  	<!-- <h2>{{ $paymentsummary->payment_data->limit_count_down }}</h2> -->

	                                        <div id="time-count-down" class="d-flex">
	                                            <div class="p-2 hours-1" id="hours-1">
	                                                0
	                                            </div>
	                                            <div class="p-2 hours-2" id="hours-2">
	                                                0
	                                            </div>
	                                            <div class="p-2 titik-dua">
	                                                :
	                                            </div>
	                                            <div class="p-2 minutes-1" id="minutes-1">
	                                                0
	                                            </div>
	                                            <div class="p-2 minutes-2" id="minutes-2">
	                                                0
	                                            </div>
	                                            <div class="p-2 titik-dua">
	                                                :
	                                            </div>
	                                            <div class="p-2 seconds-1" id="seconds-1">
	                                                0
	                                            </div>
	                                            <div class="p-2 seconds-2" id="seconds-2">
	                                                0
	                                            </div>
	                                        </div>
	                                        <div id="timeout" class="d-none">
	                                            <h3 class="c-red">WAKTU ANDA TELAH HABIS!</h3>
	                                        </div>
					  	<p style="font-size: 12px; display: block; background-color: #ff9649; color: white; text-align: center;">HARGA BERLAKU SAMPAI {{ $paymentsummary->payment_data->limit_payment }}</p>
					  	<p style="margin: auto;"><b>DESKRIPSI ACARA</b></p>
					  	<p style="font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					  	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					  	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					  	consequat.</p>
					  		<button id="payButton" class="imageContainer" style="width: 100px; height: 40px; text-align: right; color: white; cursor: pointer;">BAYAR</button>					  	
					  </div>
					</div>
				</div>
			</div>
		</form>
	</div>
@stop
@section('pm_js')
	<script type="text/javascript">
		function tab1(){
		  document.getElementById('div1').style.display ='block';
		}
		function tab2(){
		  document.getElementById('div1').style.display = 'none';
		  document.getElementById('div2').style.display = 'none';
		}
		function tab3(){
		  document.getElementById('div2').style.display = 'block';
		}
	</script>
	<script type="text/javascript">
    document.getElementById("payButton").onclick = function () {
        location.href = "{{ url('booking-complite') }}";
    };
	</script>


    <script>
            // Set the date we're counting down to
            var countDownDate = new Date('{{ $paymentsummary->payment_data->limit_payment }}').getTime();
            
            // Update the count down every 1 second
            var x = setInterval(function() {
            
                // Get todays date and time
                var now = new Date().getTime();
                
                // Find the distance between now and the count down date
                var distance = countDownDate - now;
                
                // Time calculations for days, hours, minutes and seconds
                // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                
                var hours1 = hours.toString().slice(0,1);
                var hours2 = hours.toString().slice(1,2);

                var minutes1 = minutes.toString().slice(0,1);
                var minutes2 = minutes.toString().slice(1,2);
                
                var seconds1 = seconds.toString().slice(0,1);
                var seconds2 = seconds.toString().slice(1,2);
                
                if(hours < 10) {
                    document.getElementById("hours-1").innerHTML = 0;
                    document.getElementById("hours-2").innerHTML = hours1;
                } else {
                    document.getElementById("hours-1").innerHTML = hours1;
                    document.getElementById("hours-2").innerHTML = hours2;
                }

                if(minutes < 10) {
                    document.getElementById("minutes-1").innerHTML = 0;
                    document.getElementById("minutes-2").innerHTML = minutes1;
                } else {
                    document.getElementById("minutes-1").innerHTML = minutes1;
                    document.getElementById("minutes-2").innerHTML = minutes2;
                }

                if(seconds < 10) {
                    document.getElementById("seconds-1").innerHTML = 0;
                    document.getElementById("seconds-2").innerHTML = seconds1; 
                } else {
                    document.getElementById("seconds-1").innerHTML = seconds1;
                    document.getElementById("seconds-2").innerHTML = seconds2; 
                }

                // If the count down is over, write some text 
                if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("timeout").classList.remove("d-none")
                    document.getElementById("time-count-down").classList.remove("d-flex")
                    document.getElementById("time-count-down").classList.add("d-none")
                }
            }, 1000);
    </script>
@stop
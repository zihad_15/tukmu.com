<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'COHome@index');

Route::get('hasil-pencaharian', 'SearchController@searchResult');

Route::get('cekemail-registrasi', function () {
    return view('home.success_registrasi');
});

// TRANSACTIOS / BOOKING
Route::post('/detail-event/orderBookingProses', 'COBooking@orderBookingProses');
Route::get('detail-event/{id}', 'EventController@detail');
Route::get('select-brand/{custpaymentid}', 'COBooking@selectBrand');
Route::get('/booking/{id}/{id_brand}', 'COBooking@index');
Route::get('/tambahan/{deal}/{session_chat}/{cpyid}', 'COBooking@tambahan');
Route::post('tambahanProses', 'COBooking@tambahanProses');
Route::get('metode-pembayaran/{id}', 'TransactionController@paymentMethod');
Route::post('metode-pembayaran/pembayaranProses', 'TransactionController@paymentMethodProses');
Route::get('/booking-complite/{id}', 'COBooking@complite');

// STATUS TRANSACTION
Route::get('pending-order', 'StatusController@pendingOrder');
Route::get('cicilan', 'StatusController@cicilan');
Route::get('tagihan', 'StatusController@tagihan');
Route::get('lunas', 'StatusController@lunas');

// USER NEEDS
Route::post('registrasi', 'COPendaftaran@registrasiproses');
Route::post('dologin_pengguna', 'COPendaftaran@login_pengguna');
Route::post('detail-event/dologin_pengguna', 'COPendaftaran@login_pengguna');
Route::get('logout', 'COPendaftaran@logout');

Route::get('profil-pengguna', 'UserController@index');
Route::get('daftar-profil-pengguna', 'COPendaftaran@getCityList');
Route::post('editDataProses', 'COPendaftaran@userEditDataProses');

Route::get('daftar-perusahaan-pengguna', 'COPendaftaran@editCompanyData');
Route::post('editDataProses', 'COPendaftaran@userEditDataProses');

Route::get('daftar-brand-pengguna', 'COPendaftaran@addBrandData');
Route::post('addBrandDataProses', 'COPendaftaran@addBrandDataProses');

//BRAND
Route::get('brand-detail/{id}', 'BrandController@viewBrandDetail');
Route::get('brand-edit/{id}', 'BrandController@editBrandDetail');
Route::get('deleteBrandProses/{id}', 'BrandController@deleteBrandProses');
Route::post('brand-edit/editBrandProses', 'BrandController@editBrandProses');

Route::get('loading', function () {
    return view('surat-loading.index');
});

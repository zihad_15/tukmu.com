importScripts('https://www.gstatic.com/firebasejs/5.7.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.7.1/firebase-messaging.js');

firebase.initializeApp({
    messagingSenderId: "929258258899"
});

const messaging = firebase.messaging();


messaging.setBackgroundMessageHandler(payload => {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    var notificationTitle = 'New Chat Message';
    var notificationOptions = {
        body: payload.data.body,
        icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png'
    };
    // var audioElement = document.createElement('audio');
    // audioElement.setAttribute('src', 'audio/sound.mp3');
    // audioElement.play();

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});
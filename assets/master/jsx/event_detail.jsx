var scripts =  document.getElementById('event_detail').getAttribute("src");

// myScript now contains our script object
var queryString = scripts.replace(/^[^\?]+\??/,'');

// console.log(queryString);
var res = queryString.split("=");
var res_param = queryString.split("&");

var dataurl = res[1].split("=")[0].split("&")[0];
var eventid = res_param[1].split("=")[1];

var today = new Date();

var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

if (dd < 10) {
  dd = '0' + dd;
}

if (mm < 10) {
  mm = '0' + mm;
}

today = yyyy + '-' + mm + '-' + dd ;


class EventDetail extends React.Component {
    _isMounted = false;
    isSelected = [];
    isDataSelected = [];
    constructor(props){
        super(props)
        this.state = {
            data_event: [],
            data_detail: {},
            key: 0,
            checkedItems: new Map(),
        }

        this.deleteData = this.deleteData.bind(this)
    }

    componentDidMount() {
        this._isMounted = true;
        axios.get(dataurl+"api/event/view/detail?sid="+eventid)
            .then(data => {
                if (this._isMounted) {
                    // console.log(data)
                    this.setState({
                        data_event: data.data.event_data[0].space_provide_layout,
                        data_detail: data.data.event_data[0]
                    })
                }
            })
            .catch(e => {
                console.log(e)
            })
    }

    componentWillUnmount() {
      this._isMounted = false;
    }

    changeStan(code,key){
        this.setState({
            key: key
        })
    }
    
    handleChange1(event) {
        this.setState({
            key: parseInt(event.target.value)
        });
    }

    handleChange(e) {
        const item = e.target.value.split("|");
        const isChecked = e.target.checked;
        this.setState(prevState => ({ checkedItems: prevState.checkedItems.set(item[0], isChecked) }));
        // const value = e.target.value
        // console.log(value)
        if(!isChecked) {
            this.deleteData(this.isDataSelected, item[1])
        } else {
            this.appendData(e.target.value)
        }
    }

    appendData(value) {
        this.isSelected.push(<li>{value.split("|")[1]}</li>);
        this.isDataSelected.push(value.split("|")[1])
        // console.log(this.isDataSelected)
    }
    
    deleteData(arr, value) {
        var what, a = arguments, L = a.length, ax;
        while (L > 1 && arr.length) {
            what = a[--L];
            while ((ax= arr.indexOf(what)) !== -1) {
                arr.splice(ax, 1);
            }
        }
        // console.log(arr)
        this.isSelected = []
        for(var i = 0; i < arr.length; i++){
            this.isSelected.push(<li>{arr[i]}</li>)
            // console.log(arr[i])
        }
    }

    render(){
        // console.log(this.state.data_detail)

        // const btn = this.state.data_event.map((value, i) => 
        //     <React.Fragment key={i}>
        //         <input style={{marginRight: '5px'}} className={this.state.key === i ? "btn btn-primary classevent" : "btn classevent"} onClick={() => this.changeStan(value.space_provide_layout_grid[0].code.substr(0, 1), i)} value={value.space_provide_layout_grid[0].code.substr(0, 1)} type="button" />            
        //     </React.Fragment>
        // )
        
        const btn = this.state.data_event.map((value,i) => 
            <React.Fragment key={i}>
                <option value={i}>
                    Zona {value.space_provide_layout_grid[0].code.substr(0, 1)}
                </option>
            </React.Fragment>
        )

        const content = this.state.data_event.map((v,a) => 
            v.space_provide_layout_grid.map((value,i) =>
                // console.log(value.code)
                value.grid_status == 1 || value.brandname != null ?
                    <div key={i} className={this.state.key !== a ? "d-none" : "d-flex bd-highlight"} style={{ background: 'black',color: 'white', borderBottom: '1px solid #fff'}}>
                        <div className="p-2 bd-highlight check">
                            <input disabled type="checkbox" name="gridselected[]" value={value.code}/>
                        </div>
                        <div className="p-2 bd-highlight">{value.code}</div>
                    </div>
                :
                    <div key={i} className={this.state.key !== a ? "d-none" : "d-flex bd-highlight testbg"} style={{color: 'black', borderBottom: '1px solid #fff'}}>
                        <div className="p-2 bd-highlight check">
                            {/* <input className="check-event" type="checkbox" name="gridselected[]" onChange={this.handleChange.bind(this)}  value={value.code} /> */}
                            <Checkbox value={value.layoutgridid +"|"+value.code} checked={this.state.checkedItems.get(value.layoutgridid)} onChange={this.handleChange.bind(this)} />
                        </div>
                        <div className="p-2 bd-highlight">{value.code}</div>
                    </div> 
            )
        )

        return(
            <React.Fragment>
                <div style={{margin:'auto',width:'500px', marginBottom: '15px'}}>
                    <div style={{float: 'left'}}>
                        <h3>ZONA</h3>
                        <select className="form-control" value={this.state.key} onChange={this.handleChange1.bind(this)}>
                        {btn}
                        </select>
                    </div>
                    <div className="clearfix"></div>
                </div>
                <div className="row">
                    <div className="col-md-3"></div>
                        <div className="col-md-4">
                            <div className="ex1" id="data-event-append">	
                                {content}
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="display-data" id="display-data">
                            <h3>STAN DIPILIH</h3>
                                <ul>
                                    {/* <li>A1 <i className="fas fa-times"></i></li> */}
                                    {this.isSelected}
                                </ul>
                            </div>
                        </div>
                    <div className="col-md-3"></div>
                </div>
                <br/>
                <br/>
                {/* {
                    parseInt(Date.parse(this.state.data_detail.finishdate)) > parseInt(Date.parse(today)) ?
                        <button type="button" className="btn btn-default btnSubmitNego" id="btnSubmit" style={{backgroundColor: '#ff9649', color: 'white'}}>NEGOSIASI HARGA</button>
                    :
                        null
                } */}
            </React.Fragment>
        )
    }
    
}

const Checkbox = ({ type = 'checkbox', value, checked = false, onChange }) => (
    <input type={type} name="gridselected[]" value={value} checked={checked} onChange={onChange} />
  );

ReactDOM.render(
    <EventDetail/>,
    document.getElementById('data-event')
)

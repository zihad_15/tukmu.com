var scripts =  document.getElementById('tambahan_room').getAttribute("src");

// myScript now contains our script object
var queryString = scripts.replace(/^[^\?]+\??/,'');

// console.log(queryString);
var res = queryString.split("=");
var res_param = queryString.split("&");

var dataurl = res[1].split("=")[0].split("&")[0];
var access_token = res_param[1].split("=")[1];
var url = res_param[2].split("=")[1];
var cpyid = res_param[3].split("=")[1];

const headers_ = {
    "Authorization": "Bearer "+access_token    
};

class Tambahan extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            addon_list: [],
            jumlah: [],
            checkedItems: new Map(),
        }
    }

    componentDidMount(){
        axios.get(dataurl+"api/event/view/detail/addon?cpyid="+cpyid, {headers:headers_})
        .then(data => {
            // console.log(data)
            this.setState({
                addon_list: data.data.addon_data
            })
            data.data.addon_data.map((value,i) =>
                this.setState({
                    jumlah: this.state.jumlah.concat(0)
                })
            )
        })
        .catch(e => {
            console.log(e)
        })
    }

    plus(x,i,limit) {

        let jumlah = [...this.state.jumlah];
        jumlah[i] = x + 1;
        if(x !== limit) {
            this.setState({jumlah});
        }
    }

    min(x,i,limit) {
        let jumlah = [...this.state.jumlah];
        jumlah[i] = x - 1;
        if(x !== 0){
            this.setState({jumlah});
        }
    }

    handleChange = (e) => {
        const item = e.target.name;
        const isChecked = e.target.checked;
        let jumlah_ = e.target.value;
        this.setState(prevState => ({ checkedItems: prevState.checkedItems.set(item, isChecked, jumlah_) }));
    }
    
    addOn() {
        // console.log(this.state.checkedItems)
        // this.state.jumlah.map((value,i) => 
        //     console.log(this.state.checkedItems[value][i])
        // )
        var i = 0;
        
        var form = new FormData();
        this.state.checkedItems.forEach((value, key) => {
            // console.log(`key: ${key}, value: ${value}`)
            // console.log(i-1),
            if(value === true) {
                form.append("cpyid", cpyid);
                form.append("addon["+i+"][addon_id]", key);
                form.append("addon["+i+"][addon_order]", this.state.jumlah[i]);
    
                axios.post(dataurl+"api/customer/add/order/addon", form, {headers:headers_})
                .then(data => {
                    // console.log(data)
                    if(data.data.status === "1"){
                        window.location.replace(url+"/metode-pembayaran/"+cpyid)
                    }
                })
                .catch(e => {
                    console.log(e)
                });
                // console.log("addon["+i+"][addon_order]")
            }
            i = i + 1
        });
    }

    render(){
        return(
            <React.Fragment>
                {
                    this.state.addon_list.map((value,i) => 
                        <tr className="t-a-c" key={i}>
                            <td className="p-r">
                                <div className="d-flex justify-content-start">
                                    <div className="p-2 bg-gray">
                                        <Checkbox name={value.spaceaddservid} checked={this.state.checkedItems.get(value.spaceaddservid)} value_={this.state.jumlah[i]} onChange={this.handleChange} />
                                    </div>
                                    <div className="p-2 bg-light-gray">   
                                        {value.spaceaddsrvdesc}
                                    </div>
                                </div>
                            </td>
                            <td className="p-r bg-gray">
                                <div className="d-flex justify-content-center">
                                    <div className="p-2">
                                        Rp. { value.spaceaddsrvprice }
                                    </div>
                                </div>
                            </td>
                            <td className="p-r bg-light-gray">
                                <div className="d-flex justify-content-center">
                                    <div style={{cursor:'pointer'}} className="p-2 c-white bg-orange"  onClick={() => this.min(this.state.jumlah[i], i, value.remaining)}>
                                        -
                                    </div>
                                    <div className="p-2">
                                        {this.state.jumlah[i]}
                                    </div>
                                    <div style={{cursor:'pointer'}} className="p-2 c-white bg-orange" onClick={() => this.plus(this.state.jumlah[i], i, value.remaining)}>
                                        +
                                    </div>
                                </div>
                            </td>
                            <td className="p-r bg-gray">
                                <div className="d-flex justify-content-center">
                                    <div className="p-2">
                                        { value.remaining }
                                    </div>
                                </div>
                            </td>
                            <td className="p-r bg-light-gray">
                                <div className="d-flex justify-content-center">
                                    <div className="p-2">
                                        10 hari sebelum masuk
                                    </div>
                                </div>
                            </td>
                        </tr>
                    )
                }
                <br/>
                <input type="button" className="btn btn-primary" value="TAMBAH" onClick={() => this.addOn()} />
            </React.Fragment>
        )
    }
}

const Checkbox = ({ type = 'checkbox', name, value_, checked = false, onChange }) => (
    <input type={type} name={name} value={value_} checked={checked} onChange={onChange} />
    // console.log(value_)
);


ReactDOM.render(
    <Tambahan/>,
    document.getElementById('tambahan_content')
)

var scripts =  document.getElementById('chat_room').getAttribute("src");

// myScript now contains our script object
var queryString = scripts.replace(/^[^\?]+\??/,'');

// console.log(queryString);
var res = queryString.split("=");
var res_param = queryString.split("&");

var dataurl = res[1].split("=")[0].split("&")[0];
var cpyid = res_param[1].split("=")[1];
var brand_id = res_param[2].split("=")[1];
var access_token = res_param[3].split("=")[1];
var url = res_param[4].split("=")[1];
var chat_session = res_param[5].split("=")[1];

const headers_ = {
    "Authorization": "Bearer "+access_token    
};

var config = {
    apiKey: "AIzaSyDdtiewGNa6PYaGwmQ9pxuyO-AiD2Aoevc",
    authDomain: "bukapameran-45e78.firebaseapp.com",
    databaseURL: "https://bukapameran-45e78.firebaseio.com",
    projectId: "bukapameran-45e78",
    storageBucket: "bukapameran-45e78.appspot.com",
    messagingSenderId: "929258258899"
};

firebase.initializeApp(config);
const messaging = firebase.messaging();

messaging.requestPermission()
    .then(function() {
        console.log("have Permission");
        return messaging.getToken();
    })
    .catch(function(e) {
        console.log("error ", e);
    })

messaging.getToken().then(function(token){
    // Get Chat Init
    // console.log(token)
    const fd = new FormData()
    fd.append("register_id", token)
    fd.append("authenticator", "browser")
    fd.append("cpyid", cpyid)

    axios.post("https://tukbase2.bukapameran.com/api/customer/add/nego/chat/init", fd, {headers:headers_})
        .then(data => {
            // console.log(data)
            // console.log(data.data.token.chat_session_token)
            localStorage.setItem("_chat_token", data.data.token.chat_session_token)
        })
        .catch(e => {
            console.log(e)
            if(e == "Error: Request failed with status code 401"){
                window.location.replace(url+"/logout");
            }
        })
})

const headers_chat = {
    "Authorization": "Bearer "+access_token,
    "chat-session": "Bearer "+localStorage.getItem("_chat_token")       
};


class Chat extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            // chat_session: '',
            chat_text: '',
            chat_list: [],
            loading: false,
            loading_data: false,
            loading_chat: false,
            stan_list: [],
            posisi_list: [],
            total_stan: 0,
            total_amount: 0,
            branddetail: {}
        }

        this.handleChangeChat = this.handleChangeChat.bind(this)
    }

    componentDidMount(){
        this.getDataChat()
        this.getDataStan()
        this.getDataStan1()
        this.getDataBrandDetail()

        // Automatic reload 5 seconds
        //  setInterval(() => {
        //      this.getDataChat()
            // console.log(this.state.chat_list)
        // }, 5000)
        messaging.onMessage(payload => {
                // console.log('onMessage:', payload)
                new Notification('New Chat Message', {
                    icon: 'https://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
                    body: payload.data.body,
                });
                var audioElement = document.createElement('audio');
                audioElement.setAttribute('src', url+'/assets/master/audio/sound.mp3');
                audioElement.play();

                axios.get(dataurl+'api/customer/view/order/booking/pending/spec?cpyid='+cpyid,{headers:headers_})
                    .then(data => {
                        this.setState({
                            chat_list: data.data.nego_chat
                        })
                    })
                    .catch(e => {
                        // console.log(e)
                    })

                
                axios.get(dataurl+'api/customer/view/order/booking/pending/spec?cpyid='+cpyid,{headers:headers_chat})
                    .then(data => {
                        // console.log(data)
                        this.setState({
                            stan_list: data.data.payment_grid_with_code
                        })
                    })
                    .catch(e => {
                        console.log(e)
                    })

                axios.get(dataurl+'api/customer/view/order/booking/pending/spec?cpyid='+cpyid,{headers:headers_chat})
                    .then(data => {
                        // console.log(data)
                        this.setState({
                            posisi_list: data.data.payment_grid_with_code,
                            total_stan: data.data.payment_grid_with_code.length
                        })

                        // var t = data.data.payment_grid_with_code.filter( obj => obj.name === 'dealvalue')[0];
                        // console.log(t)
                        var total = [];
                        data.data.payment_grid_with_code.map((value, i) => {
                            // console.log(value.dealvalue)
                            total.push(parseInt(value.dealvalue))
                        })

                        // console.log(total)
                        var sum = total.reduce((a, b) => a + b, 0);
                        // console.log(sum);
                        this.setState({
                            total_amount: this.convertToRupiah(sum)
                        })
                    })
                    .catch(e => {
                        console.log(e)
                    })
            })
    }

    componentDidUpdate() {
        // get the messagelist container and set the scrollTop to the height of the container
        if(!this.state.loading_data) {
            const objDiv = document.getElementById('chat');
            objDiv.scrollTop = objDiv.scrollHeight;
        }
    }

    getDataBrandDetail(){
        this.setState({
            loading_data: true
        })
        axios.get(dataurl+'api/customer/view/order/booking/pending/spec?cpyid='+cpyid,{headers:headers_})
            .then(data => {
                // console.log(data.data)
                this.setState({
                    branddetail: {
                        startdate: data.data.startdate,
                        finishdate: data.data.finishdate,
                        spacename: data.data.spacename,
                        minprice: data.data.minprice,
                        maxprice: data.data.maxprice,
                        brandname: data.data.customer_brand.brandname,
                        payment_grid_with_code: [data.data.payment_grid_with_code],
                        code: data.data.payment_grid_with_code[0].code,
                        customer_brand: data.data.customer_brand,
                        desc: data.data.desc
                    },
                    loading_data: false
                })
            })
            .catch(e => {
                // console.log(e)
                this.setState({
                    loading_data: false
                })
            })
    }

    getDataChat() {
        this.setState({
            loading_chat: true
        })
        axios.get(dataurl+'api/customer/view/order/booking/pending/spec?cpyid='+cpyid,{headers:headers_})
            .then(data => {
                // console.log(data)
                this.setState({
                    chat_list: data.data.nego_chat,
                    loading_chat: false
                })
            })
            .catch(e => {
                // console.log(e)
                this.setState({
                    loading_chat: false
                })
            })
    }

    handleChangeChat(event) {
        this.setState({chat_text: event.target.value});
    }

    sendChat() {
        // this.setState({
        //     chat_list: this.state.chat_list.concat(this.state.chat_text)
        // })
        
        const fd = new FormData()
        fd.append("cust_chat", this.state.chat_text)
        fd.append("cpyid", cpyid)
        fd.append("chat_type", 0)

        this.setState({
            loading: true
        })

        axios.post(dataurl+"api/customer/add/nego/chat/send", fd, {headers:headers_chat})
        .then(data => {
            // console.log(data)
            if(data.data.status === "1"){
                axios.get(dataurl+'api/customer/view/order/booking/pending/spec?cpyid='+cpyid,{headers:headers_})
                .then(data => {
                    // console.log(data)
                    this.setState({
                        chat_list: data.data.nego_chat,
                        chat_text: '',
                        loading: false
                    })
                })
                .catch(e => {
                    console.log(e)
                    this.setState({
                        loading: false
                    })
                })
            }
        })
        .catch(e => {
            // console.log(e)
        })
        // console.log(fd)
    }

    getDataStan() {

        axios.get(dataurl+'api/customer/view/order/booking/pending/spec?cpyid='+cpyid,{headers:headers_chat})
            .then(data => {
                // console.log(data)
                this.setState({
                    stan_list: data.data.payment_grid_with_code
                })
            })
            .catch(e => {
                console.log(e)
            })
    }

    getDataStan1() {

        axios.get(dataurl+'api/customer/view/order/booking/pending/spec?cpyid='+cpyid,{headers:headers_chat})
            .then(data => {
                // console.log(data)
                this.setState({
                    posisi_list: data.data.payment_grid_with_code,
                    total_stan: data.data.payment_grid_with_code.length
                })

                // var t = data.data.payment_grid_with_code.filter( obj => obj.name === 'dealvalue')[0];
                // console.log(t)
                var total = [];
                data.data.payment_grid_with_code.map((value, i) => {
                    // console.log(value.dealvalue)
                    total.push(parseInt(value.dealvalue))
                })

                // console.log(total)
                var sum = total.reduce((a, b) => a + b, 0);
                // console.log(sum);
                this.setState({
                    total_amount: this.convertToRupiah(sum)
                })
            })
            .catch(e => {
                console.log(e)
            })
    }

    convertToRupiah(angka) {
        var rupiah = '';		
        var angkarev = angka.toString().split('').reverse().join('');
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
        return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
    }
    
    formatDate(date) {

        var monthNames = [
            "Januari", "Februari", "Maret",
            "April", "Mei", "Juni", "Juli",
            "Augustus", "September", "Oktober",
            "November", "Desember"
        ];

        var hari = [
            "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"
        ];

        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = this.addZero(hours) + ':' + this.addZero(minutes) + ' ' + ampm;

        var hariIndex = date.getDay();
        var tanggal = this.addZero(date.getDate());
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return tanggal + ' ' + monthNames[monthIndex] + ' ' + year;
    }
    
    addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }


    render(){
        // console.log(this.state.chat_session)
        // console.log(this.state.chat_list)
        // console.log(this.state.branddetail)
        const { branddetail, loading_data, loading_chat } = this.state;
        
        const ListChat = this.state.chat_list.map((value,i) =>
            <div key={i}>
                {
                    value.sp_name === null && value.cust_name !== null && value.chatvalue != "" ?
                        <div className="d-flex bd-highlight content-chat" style={{margin: '0px 20px'}}>
                            <div className="p-2 bd-highlight people " style={{padding: '10px', borderRadius: '100px'}}>
                                <i className="far fa-user"></i>
                            </div>
                            <div className="p-2 flex-grow-1 bd-highlight text-chat" style={{border: '1px solid #000', marginBottom: '30px', borderRadius: '10px'}}>
                                <p>{value.chatvalue}</p>
                            </div>
                        </div>
                    : null
                }
                {/* {
                    value.cust_name === null && value.sp_name !== null ?
                        <div className="d-flex bd-highlight align-content-end content-chat-answer" style={{margin: '0px 20px'}}>
                            <div style={{paddingTop: '0px !important'}} className="p-2 flex-grow-1 bd-highlight text-chat" style={{boxShadow: '3px 3px 3px #000000', 'border': '1px solid #000', marginBottom: '30px', borderRadius: '10px'}}>
                                <p style={{textAlign: 'right'}}>
                                    {value.chatvalue}
                                </p>
                            </div>
                            <div className="p-2bd-highlight people " style={{height: '50px', border: '2px solid #000', padding: '10px', borderRadius: '50px'}}>
                                <i className="fas fa-users"></i>
                            </div>
                        </div>
                    : null
                } */}
                {
                    value.cust_name === null && value.sp_name === null && value.chatvalue != "" ?
                        <div className="d-flex bd-highlight align-content-end content-chat-answer" style={{margin: '0px 20px'}}>
                            <div style={{paddingTop: '0px !important'}} className="p-2 flex-grow-1 bd-highlight text-chat" style={{boxShadow: '3px 3px 3px #000000', 'border': '1px solid #000', marginBottom: '30px', borderRadius: '10px'}}>
                                <p style={{color: 'red', textAlign: 'center'}}>SYSTEM</p>
                                <p style={{textAlign: 'center'}}>{value.chatvalue}</p>
                            </div>
                            <div className="p-2bd-highlight people ">
                            </div>
                        </div>
                    : null
                }
                {
                    value.cust_name === null && value.sp_name !== null && value.chatvalue != ""  ?
                        <form action="" method="POST">
                            <div className="d-flex bd-highlight align-content-end content-chat-answer" style={{margin: '0px 20px'}}>
                                <div style={{paddingTop: '0px !important'}} className="p-2 flex-grow-1 bd-highlight text-chat" style={{border: '1px solid #000', marginBottom: '30px', borderRadius: '10px'}}>
                                    <p style={{textAlign: 'right'}}>{value.chatvalue}</p>
                                    {
                                        value.chattype === 3 ?
                                            <div>
                                                <span>Rp. {value.chatparams.total}</span><br/>
                                                <a href={url+"/tambahan/1/"+chat_session+"/"+cpyid}>YES</a>
                                                {/* <a href="">NO</a> */}
                                            </div>
                                        : null
                                    }
                                </div>
                                <div className="p-2bd-highlight people " style={{height: '50px', padding: '10px', borderRadius: '100px'}}>
                                    <i className="far fa-user"></i>
                                </div>
                            </div>
                        </form>
                    : null
                }
            </div>
        );

        const stan = this.state.stan_list.map((value,i) =>
            <div key={i} style={{marginRight: '2px'}}>
                {value.code},
            </div>
        )

        const posisi = this.state.posisi_list.map((value,i) =>
            <tr key={i}>
                <td className="bg-light-gray">{value.code}</td>
                <td className="bg-light-gray">{this.convertToRupiah(parseInt(value.dealvalue))}</td>
            </tr>
        )

        return(
            <React.Fragment>
                {
                    loading_data ?
                        <div style={{width: "300px", margin: "100px auto"}}>
                            <p style={{textAlign: 'center'}}>
                                <i className="fas fa-spinner fa-spin" style={{fontSize:'90px', textAlign: 'center'}}></i>                                                        
                            </p>
                        </div>
                    : 
                        <div className="container content m-t-15 m-b-15">
                            <div className="row">
                                <div className="col-md-12">
                                    <h2 className="f-w-300">PEMESANAN</h2>
                                    <div className="row content-booking">
                                        <div className="col-md-6">
                                            <div className="bg-white p-15 m-b-15">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <h2 className="f-w-300">JUDUL</h2>
                                                        <br/>
                                                        <div className="d-flex">
                                                            <div className="p-2 p-r-l">
                                                                <i className="fa fa-calendar-alt c-orange"></i>
                                                            </div>
                                                            <div className="p-2 p-r-l">
                                                                <span>{ this.formatDate(new Date(branddetail.startdate)) } - { this.formatDate(new Date(branddetail.finishdate)) }</span>
                                                            </div>
                                                        </div>
                                                        <div className="d-flex">
                                                            <div className="p-2 p-r-l">
                                                                <i className="fa fa-map-marker-alt c-orange"></i>
                                                            </div>
                                                            <div className="p-2 p-r-l">
                                                                <span>{ branddetail.spacename }</span>
                                                            </div>
                                                        </div>
                                                        <div className="d-flex">
                                                            <div className="p-2 p-r-l">
                                                                <i className="fa fa-dollar-sign c-orange"></i>
                                                            </div>
                                                            <div className="p-2 p-r-l">
                                                                <span>Rp. { branddetail.minprice } - Rp. { branddetail.maxprice }</span>
                                                            </div>
                                                        </div>
                                                        <br/>
                                                        <h6 className="m-r-b f-s-14">STAN</h6>
                                                        <span className="c-orange t-td-u">
                                                            {/* { branddetail.code } */}
                                                            {/* {
                                                                branddetail.payment_grid_with_code.map((value,i) =>
                                                                    <div key={i}>
                                                                        {value.code}
                                                                    </div>
                                                                )
                                                            } */}
                                                            <div id="stan" style={{display:'flex'}}>
                                                                {stan}
                                                            </div>
                                                        </span>
                                                        <hr/>
                                                        { branddetail.customer_brand !== null ?
                                                        <React.Fragment>
                                                            <h6 className="m-r-b f-s-14">BRAND</h6>
                                                            <span className="c-orange">{ branddetail.brandname }</span>
                                                        </React.Fragment>
                                                        : null
                                                        }
                                                        <hr/>
                                                        <h6 className="m-r-b f-s-14">PENGELOLA</h6>
                                                        <span className="c-orange t-td-u"></span>
                                                        <hr />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <img className="img-fluid img-thumbnail" src={url+"/assets/master/images/beatles.png"} />
                                                        <br/><br/>
                                                        <h6 className="m-r-b">KONDISI & SYARAT</h6>
                                                        <a className="c-orange t-td-u" href="#"><span>LIHAT</span></a>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <h6>TENTANG ACARA</h6>
                                                        <p className="f-s-14" dangerouslySetInnerHTML={{__html: branddetail.desc}} />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="bg-white p-15">
                                                <h2 className="m-r-b">TAMBAHAN</h2>
                                                <span className="c-orange f-s-12"><i>Anda baru bisa memilih tambahan setelah menyelesaikan negosiasi</i></span>
        
                                                <div className="table-responsive-md m-t-15 m-b-15">
                                                    <table className="table">
                                                        <thead>
                                                            <tr className="bg-orange c-white t-a-c">
                                                                <td>JENIS</td>
                                                                <td>HARGA</td>
                                                                <td>SISA</td>
                                                                <td>BATAS PESAN</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr className="t-a-c">
                                                                <td className="bg-light-orange">Umbul 80 x 190</td>
                                                                <td className="bg-light-yellow">Rp. 150.000</td>
                                                                <td className="bg-light-orange">5</td>
                                                                <td className="bg-light-yellow">01 jan 18</td>
                                                            </tr>
                                                            <tr className="t-a-c">
                                                                <td className="bg-light-orange">Litrik / 2 Amper</td>
                                                                <td className="bg-light-yellow">Rp. 150.000</td>
                                                                <td className="bg-light-orange">5</td>
                                                                <td className="bg-light-yellow">01 jan 18</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
        
                                                <h6 className="m-r-b">Keterangan</h6>
                                                <span className="f-s-12">- Harga diatas berlaku dari tanggal 08 Desember - 10 Desember 2017</span>
                                            </div>
                                        </div>
                                        <div className="col-md-6 p-r-l chat-content">
                                            <div className="bg-light-black p-15">
                                                <div className="b-radius-15 p-b-15 p-r-15 p-l-15 bg-dark-gray">
                                                    <h3 className="t-a-c f-w-300 c-white">PESAN</h3>
                                                    <div id="chat_content">
                                                        <ul className="nav nav-tabs tabs-pesan" id="myTab" role="tablist">
                                                            <li className="nav-item bg-light-gray b-radius-t-10">
                                                                <a className="nav-link active c-black" id="home-tab" data-toggle="tab" href="#chat" role="tab" aria-controls="chat" aria-selected="true">CHAT</a>
                                                            </li>
                                                            <li className="nav-item bg-light-gray b-radius-t-10">
                                                                <a className="nav-link c-black" id="profile-tab" data-toggle="tab" href="#dokumen" role="tab" aria-controls="dokumen" aria-selected="false">DOKUMEN</a>
                                                            </li>
                                                            <li className="nav-item bg-light-gray b-radius-t-10">
                                                                <a className="nav-link c-black" id="contact-tab" data-toggle="tab" href="#link" role="tab" aria-controls="link" aria-selected="false">LINK</a>
                                                            </li>
                                                        </ul>
                                                        <div className="tab-content content-pesan" id="myTabContent">
                                                            <div className="tab-pane fade show active p-5" id="chat" role="tabpanel" aria-labelledby="chat-tab" style={{height: '400px', overflowY: 'scroll'}}>
                                                                {
                                                                    loading_chat ?
                                                                    <p style={{textAlign:'center'}}>
                                                                        <i className="fas fa-spinner fa-spin" style={{fontSize:'40px', textAlign: 'center'}}></i>                                                                                                                                
                                                                    </p>
                                                                    : 
                                                                    ListChat
                                                                }
                                                            </div>
                                                            <div className="tab-pane fade p-5" id="dokumen" role="tabpanel" aria-labelledby="dokumen-tab" style={{height: '400px', overflowY: 'scroll'}}>
                                                                ...
                                                            </div>
                                                            <div className="tab-pane fade p-5" id="link" role="tabpanel" aria-labelledby="link-tab" style={{height: '400px', overflowY: 'scroll'}}>
                                                                ...
                                                            </div>
                                                        </div>
                                                        <div className="p-5 bg-white m-t-5 b-radius-10 papan-ketik-pesan">
                                                            <div className="d-flex bd-highlight">
                                                                <div className="p-2 flex-grow-1 bd-highlight">
                                                                    <textarea style={{borderTop: '1px solid #bebebe', borderBottom:'1px solid #bebebe',
                                                                        borderRight:'1px solid #bebebe', borderLeft:'1px solid #bebebe', borderRadius: '5px'}}  className="form-control text-area" rows="2" value={this.state.chat_text} onChange={this.handleChangeChat} />
                                                                </div>
                                                                <div className="p-2 bd-highlight p-r-r">
                                                                    <button className="link">
                                                                        <i className="fa fa-link c-orange"></i>
                                                                    </button>
                                                                </div>
                                                                <div className="p-2 bd-highlight p-r-r">
                                                                    <button className="link">
                                                                        <i className="fa fa-paperclip c-orange"></i>
                                                                    </button>
                                                                </div>
                                                                <div className="p-2 bd-highlight">
                                                                    {
                                                                        this.state.loading ? 
                                                                            <i className="fas fa-spinner fa-spin" style={{fontSize:'30px'}}></i>
                                                                        : 
                                                                        <button className="link bg-orange" onClick={() => this.sendChat()}>
                                                                            <i className="fa fa-paper-plane c-white"></i>
                                                                        </button>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
        
                                                
                                                <React.Fragment>
                                                    <div className="table-responsive m-t-15">
                                                        <table className="table t-a-c m-r-b">
                                                            <thead>
                                                                <tr className="bg-orange c-white">
                                                                    <td>NO. POSISI</td>
                                                                    <td>HARGA</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {posisi}
                                                            </tbody>
                                                        </table>
                                                    </div>
        
                                                    <div className="table-responsive">   
                                                        <table className="table m-r-b">
                                                            <tbody>
                                                                <tr className="bg-gray c-white">
                                                                    <td>TOTAL STAN</td>
                                                                    <td className="t-a-r">{this.state.total_stan}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="table-responsive">   
                                                        <table className="table m-r-b">
                                                            <tbody>
                                                                <tr className="bg-light-black c-white">
                                                                    <td className="b-r-top">
                                                                        <i>
                                                                            LAIN LAIN<br/>
                                                                            DEPOSIT
                                                                        </i>
                                                                    </td>
                                                                    <td className="t-a-r b-r-top">
                                                                        0<br/>
                                                                        0
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <hr className="b-orange m-r"/>
                                                    <div className="table-responsive">   
                                                        <table className="table m-r-b">
                                                            <tbody>
                                                                <tr className="bg-light-black c-white">
                                                                    <td className="b-r-top">
                                                                        <h6 className="m-r-b">TOTAL TAGIHAN</h6>
                                                                    </td>
                                                                    <td className="t-a-r b-r-top">
                                                                        {this.state.total_amount}
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </React.Fragment>
        
                                                <hr className="b-orange m-r"/>
                                                <div className="table-responsive">   
                                                    <table className="table m-r-b">
                                                        <tbody>
                                                            <tr className="bg-light-black c-white">
                                                                <td className="b-r-top">
                                                                    <span className="c-orange t-td-u f-s-12">
                                                                        <i>detail cicilan</i>
                                                                    </span>
                                                                </td>
                                                                {/* <td className="t-a-r b-r-top">
                                                                    <span className="c-orange f-s-12">
                                                                        Time remaining: 03:59:59
                                                                    </span>
                                                                </td> */}
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                {/* <div className="d-flex bd-highlight mb-3">
                                                    <div className="p-2 bd-highlight">
                                                        <button className="btn btn-pesan b-radius-15 b-r bg-green f-s-12 c-white" data-toggle="modal" data-target="#modalconfirmsetuju">
                                                            SETUJU
                                                        </button>
                                                    </div>
                                                    <div className="p-2 bd-highlight">
                                                        <button className="btn btn-pesan b-radius-15 b-r bg-red f-s-12 c-white" data-toggle="modal" data-target="#modalconfirmtidaksetuju">
                                                            TIDAK
                                                        </button>
                                                    </div>
                                                    <div className="ml-auto p-2 bd-highlight">
                                                        <button className="btn btn-pesan b-radius-15 b-r bg-red f-s-12 c-white" data-toggle="modal" data-target="#modalconfirmbatalpesanan">
                                                            BATAL 
                                                        </button>
                                                    </div>
                                                </div> */}
        
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                }
            </React.Fragment>
        );
    }
}

ReactDOM.render(
    <Chat/>,
    document.getElementById('chat_content')
)

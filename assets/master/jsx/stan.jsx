var scripts =  document.getElementById('stan_room').getAttribute("src");

// myScript now contains our script object
var queryString = scripts.replace(/^[^\?]+\??/,'');

// console.log(queryString);
var res = queryString.split("=");
var res_param = queryString.split("&");

var dataurl = res[1].split("=")[0].split("&")[0];
var cpyid = res_param[1].split("=")[1];
var brand_id = res_param[2].split("=")[1];
var access_token = res_param[3].split("=")[1];
var url = res_param[4].split("=")[1];
var chat_session = res_param[5].split("=")[1];

const headers_ = {
    "Authorization": "Bearer "+access_token    
};

class Stan extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            harga: '',
            chat_session: '',
            stan_list: []
        }

    }

    componentDidMount(){

        // const fd = new FormData()
        // fd.append("register_id", "AAAA2FwbwdM:APA91bFPqMP4e2IdJ3EB_bx_8hKRnSWa3Uc_FXu0o_cuo69H14N4DvYd70yNoCiIcXnw86J1OBxcs4ng_BJK7YREukI8hoPnp9t9O49_5LetUkVLPEdpECHlygNrWf4Xm3JaRvTf2Xre")
        // fd.append("authenticator", "browser")
        // fd.append("cpyid", cpyid)

        // axios.post(dataurl+"api/customer/add/nego/chat/init", fd, {headers:headers_})
        // .then(data => {
        //     // console.log(data)
        //     this.setState({
        //         chat_session: data.data.token.chat_session_token
        //     })
        // })
        // .catch(e => {
        //     console.log(e)
        // })
        // console.log(this.state.chat_session)
        this.getDataStan()

        // Automatic reload 5 seconds
        // setInterval(() => {
        //     this.getDataStan()
        // }, 5000)

    }

    getDataStan() {

        const headers_chat = {
            "Authorization": "Bearer "+access_token,
            "chat-session": "Bearer "+chat_session       
        };

        axios.get(dataurl+'api/customer/view/order/booking/pending/spec?cpyid='+cpyid,{headers:headers_chat})
            .then(data => {
                // console.log(data)
                this.setState({
                    stan_list: data.data.payment_grid_with_code
                })
            })
            .catch(e => {
                console.log(e)
            })
    }

    render(){
        const stan = this.state.stan_list.map((value,i) =>
            <div key={i}>
                {value.code}<br/>
            </div>
        )
        return(
            <div>
                {stan}
            </div>
        )
    }


}

ReactDOM.render(
    <Stan/>,
    document.getElementById('stan')
)